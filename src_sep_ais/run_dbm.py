#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import time
import os
import numpy as np

from keras.optimizers import SGD, Nadam
from keras.layers import Input
from keras.regularizers import WeightRegularizer
from keras import callbacks

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/keras_boltzmann/src/'
        os.chdir(path_to_script_folder)

from my_callbacks import PeriodicSave, AISCallback, SampleCallback, PlotCallback, EpochEnd, MomentumRateScheduler
from models import ModelUnsupervised
from dbm import DBM_Res
import my_utils
import my_data

#%%
def momentum_function(mom_init = 0.5, mom_final = 0.9, epoch_final = 5):
    
    def schedule(epoch):
        
        if epoch < epoch_final:
            momentum = mom_init
        else:
            momentum = mom_final
        
        return momentum
    
    return schedule

#%%
def main(layer_size_list, topology_dict, residual_dict, save_folder,
         dataset = 'MNIST', weight_file = None,
         mean_field = True, persist = True, centered = False, pos_steps = 1,
         neg_steps = 5, L1_reg = 1e-5, L2_reg = 1e-5,
         dropout_p = [], mbs = 128, nb_epochs = 250, optimizer={},
        mom_schedule = None):
    
    ##### Prep for the nnet

    save_folder = my_utils.standard_folder(save_folder)
    my_utils.safe_make_folders(save_folder)
    weight_folder = save_folder + 'weights/'
    sample_folder = save_folder + 'samples/'
    plot_folder = save_folder + 'plots/'
    os.makedirs(weight_folder)
    os.makedirs(sample_folder)
    os.makedirs(plot_folder)

    data = my_data.Data(dataset,
                        valid_split = 0.2,
                        index_dict = {},
                        shuffle_trainvalid = False,
                        feature_wise_mean = False,
                        feature_wise_std = False,
                        PCA_transform = False,
                        ZCA_transform = False,
                        prob_norm = True,
                        weight_dict = {},
                        ImageDataGen_dict = {})
    data.save(save_folder+'data.txt')

    # setup model structure
    input_layer = Input(shape=(layer_size_list[0],))
    
    W_regularizer = WeightRegularizer(l1=L1_reg, l2=L2_reg)
    if weight_file is not None:
        weights_dict = my_utils.load_weights_hd5f(weight_file)
    else:
        weights_dict = None
        
    
    # this helps speed up training
    index = np.random.choice(np.arange(data.train.num_samples),size=mbs,replace=False)
    init_mbs = data.train.data_flat[index]
    eps = 100*np.finfo(np.float32).eps
    vis_mean = np.clip(np.mean(data.train.data_flat, axis=0), eps, 1-eps)
    
    ##### Create the model
    
    dbm = DBM_Res(layer_size_list = layer_size_list,
              topology_dict = topology_dict, residual_dict = residual_dict,
              dropout_prob_list = dropout_p,
              mean_field = mean_field, W_regularizer=W_regularizer,
              persist = persist, mbs = mbs, centered = centered,
              weights_dict = weights_dict, init_mbs = init_mbs, vis_mean = vis_mean)

    model = ModelUnsupervised(input_layer, dbm.layers)
    
    # setup optimizer, loss
    if optimizer['name'] == 'nadam':
        try:
            lr = optimizer['lr']
        except KeyError:
            lr = 0.002
            optimizer['lr'] = lr
        try:
            beta_1 = optimizer['beta_1']
        except KeyError:
            beta_1 = 0.9
            optimizer['beta_1'] = beta_1
        try:
            beta_2 = optimizer['beta_2']
        except KeyError:
            beta_2 = 0.999
            optimizer['beta_2'] = beta_2
        try:
            epsilon = optimizer['epsilon']
        except KeyError:
            epsilon = 1e-08
            optimizer['epsilon'] = epsilon
        try:
            schedule_decay = optimizer['schedule_decay']
        except KeyError:
            schedule_decay = 0.004
            optimizer['schedule_decay'] = schedule_decay            
                     
        opt = Nadam(lr=lr, beta_1=beta_1, beta_2=beta_2,
                    epsilon=epsilon, schedule_decay=schedule_decay)
        
    elif optimizer['name'] == 'sgd':
        lr = optimizer['lr']
        try:
            mom = optimizer['mom']
        except KeyError:
            mom = 0.99
            optimizer['mom'] = mom
        try:
            decay = optimizer['decay']
        except KeyError:
            decay = 5e-3
            optimizer['decay'] = decay
        try:
            nesterov = optimizer['nesterov']
        except KeyError:
            nesterov = True
            optimizer['nesterov'] = nesterov
        
        opt = SGD(lr, mom, decay = decay, nesterov = nesterov)    
        
    else:
        raise NotImplementedError

    # metrics of interest
    cd = dbm.contrastive_divergence_loss(pos_steps = pos_steps, neg_steps = neg_steps)
    
    # compile the model
    model.compile(optimizer=opt, loss=cd, metrics=[])
    
    ###### Callbacks
    recon = dbm.reconstruction_loss(pos_steps = pos_steps, neg_steps = neg_steps)
    fe = dbm.free_energy_loss()
    
    outputs_dict = {'recon': recon, 'free_energy': fe}
    
    cb_end = EpochEnd(outputs_dict, data.train.data_flat, epoch_ls = [])
    
    cb_csv = callbacks.CSVLogger(save_folder + 'csv.txt', append=False)
    
    cb_ais =  AISCallback(dbm, data.train.data_flat,
                          nb_runs = 100, nb_betas = 1000, epoch_ls = [])
    
    epoch_ls = list(set(list(range(0, nb_epochs, 50)) + [1,5] + [nb_epochs-1]))
    cb_ais_long =  AISCallback(dbm, data.train.data_flat,
                               nb_runs = 1000, nb_betas = 30000,
                               epoch_ls = epoch_ls, name='detailed')
    
    cb_sample = SampleCallback(dbm, sample_folder, 20, 10, 2000, epoch_ls = epoch_ls)
   
    cb_mc = callbacks.ModelCheckpoint(weight_folder + 'weights.best.hdf5',
                                            monitor='val_prob',
                                            verbose=0, save_best_only=True,
                                            save_weights_only=True,
                                            mode='max', period=1)
    
    cb_es = callbacks.EarlyStopping(monitor='val_prob', min_delta=0,
                                          patience=100, verbose=0, mode='max')
    
    cb_ps = PeriodicSave(weight_folder + 'weights.{epoch:03d}.hdf5' , epoch_ls = epoch_ls)
    
    
    cb_plot = PlotCallback(save_folder = plot_folder,
                           csv_filepath = save_folder + 'csv.txt')
    
    callback_list = [cb_end, cb_ais, cb_ais_long, cb_sample,
                     cb_plot, cb_ps, cb_mc, cb_es, cb_csv]
    
    #callback_list = [cb_end, cb_csv]
    
    if mom_schedule is not None:
        cb_mom = MomentumRateScheduler(mom_schedule)
        callback_list += [cb_mom]
        

    ###### Save parameters used for training
    param = {'layer_size_list' : layer_size_list,
             'topology_dict' : topology_dict,
             'residual_dict' : residual_dict,
             'save_folder' : save_folder,
             'dataset' : dataset,
             'weight_file' : weight_file,
             'mean_field' : mean_field,
             'persist' : persist,
             'centered' : centered,
             'pos_steps' : pos_steps,
             'neg_steps' : neg_steps,
             'L1_reg' : L1_reg,
             'L2_reg' : L2_reg,
             'dropout_p' : dropout_p,
             'mbs' : mbs,
             'nb_epochs' : nb_epochs,
             'optimizer' : optimizer
             }
    my_utils.save_dict(save_folder + 'param.txt', param)
   
    ##### Training
    start = time.time()
    history = model.fit(data.train.data_flat, batch_size = mbs,
                        nb_epoch = nb_epochs, verbose=0, shuffle=True,
                        validation_data = data.valid.data_flat, callbacks=callback_list)
    total = time.time()-start
            
    with open(save_folder+'runtime.txt', 'w') as f:
        f.write('Total runtime was {} minutes.\n'.format(total/60.0))
        f.write('Per epoch runtime was {} seconds.'.format(total/nb_epochs))
            
#%%
if __name__ == '__main__':
    
    layer_size_list = [784]+ 2*[500]
    dropout_p = [0.0]*len(layer_size_list)
    topology_dict = {0:{1}, 1:{2}}
    #topology_dict = {0:{1}, 1:{2,4}, 2:{3}, 3:{4}, 4:{5}}
    residual_dict = {}
    
    save_folder = '../results/melchior_v3'
    optimizer = {'name': 'nadam'}
    #optimizer = {'name': 'sgd', 'lr' : 0.001,
    #               'mom' : 0.9, 'decay' : 1e-3, 'nesterov' : True}
    #optimizer = {'name': 'sgd', 'lr' : 0.001,
    #               'mom' : 0.9, 'decay' : 0.0, 'nesterov' : True}

    mom_schedule = None

    nb_epochs = 500
    
    weight_file = None
    dataset = 'MNIST_binary'
    mean_field = True
    persist = False
    centered = True
    pos_steps = 1
    neg_steps = 5
    L1_reg = 1e-5
    L2_reg = 1e-5
    mbs = 128    
    
    main(layer_size_list, topology_dict, residual_dict, save_folder,
         dataset, weight_file,
         mean_field, persist, centered,
         pos_steps, neg_steps, L1_reg, L2_reg, dropout_p,
         mbs, nb_epochs, optimizer, mom_schedule)
