#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os

import six
import numpy as np

from keras import backend as K 
import theano

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/keras_boltzmann/src/'
        os.chdir(path_to_script_folder)

from my_utils import binomial_sample
from dbm import DBM, DBM_Res

#%%
def logsum(x):
    # This prevents overflow
    a = np.max(x)
    ls = a + np.log(np.sum(np.exp(x-a)))
    return ls

#%%
def logdiff(x):
    # This prevents overflow
    assert x.shape[0]==2
    a = np.max(x)
    ls = a + np.log(np.diff(np.exp(x-a)))
    return ls.item()

#%%
def ais_estimate_logz(log_w, logZ_base):


    nb_runs = log_w.shape[0]
    r_AIS = logsum(log_w) -  np.log(nb_runs) 
    aa = np.mean(log_w)
    logstd_AIS = np.log(np.std(np.exp(log_w-aa))) + aa - np.log(nb_runs)/2.0
    # Same as computing:
    # logstd_AIS = np.log(np.std(np.exp(log_w))/np.sqrt(nb_runs))     
    
    logZ_est = r_AIS + logZ_base
    
    l_input = np.zeros((2,))
    l_input[0] = np.log(3)+logstd_AIS
    l_input[1] = r_AIS
    
    logZ_est_up = logsum(l_input) + logZ_base
    
    logZ_est_down = logdiff(l_input) + logZ_base
    if np.isnan(logZ_est_down):
        logZ_est_down = 0

    return logZ_est, logZ_est_up, logZ_est_down
 
#%%
class AIS(object):

    def __init__(self, dbm):
        
        weights = dbm.get_weights()
        weights_dict = {}
        for w in weights:
            weights_dict[w.name] = w
        if dbm.centered:
            b = dbm.layers[1].b
            W = weights_dict['h1-h0_W']
            c = dbm.layers[0].c
            weights_dict['h1_b'] = b - K.dot(c, W)
            
        for key, value in six.iteritems(weights_dict): 
            weights_dict[key] = value.eval()
        
        if isinstance(dbm, DBM):
            self.dbm =     DBM(dbm.layer_size_list,
                               topology_dict = dbm.topology_dict,
                               mean_field = dbm.mean_field,
                               persist = False,
                               centered = False,
                               dropout_prob_list = None,
                               mbs = dbm.mbs,
                               weights_dict = weights_dict,
                               theano_rng = dbm.theano_rng,
                               init_mbs = None,
                               vis_mean = None)
            
        elif isinstance(dbm, DBM_Res):
            self.dbm = DBM_Res(dbm.layer_size_list,
                               topology_dict = dbm.topology_dict,
                               residual_dict = dbm.residual_dict,
                               mean_field = dbm.mean_field,
                               persist = False,
                               centered = False,
                               dropout_prob_list = None,
                               mbs = dbm.mbs,
                               weights_dict = weights_dict,
                               theano_rng = dbm.theano_rng,
                               init_mbs = None,
                               vis_mean = None)
        
    
    def ais_fxn(self, x, nb_runs = 100, nb_betas = 1e3):
        """
        Annealed importance sampling
        """
        
        inputs = [x]
        
        if self.dbm.uses_learning_phase and not isinstance(K.learning_phase(), int):
            inputs += [K.learning_phase()]
        
        # this is similar to the proportions used by Ruslan
        num_low = int(0.035*nb_betas)
        num_mid = int(0.275*nb_betas)
        num_high = nb_betas - num_low - num_mid
        betas = np.hstack((np.linspace(0, 0.5, num_low),
                           np.linspace(0.5, 0.9, num_mid),
                           np.linspace(0.9, 1, num_high)))
        
        betas = K.variable(betas, dtype = K.floatx(), name='betas')

        log_w = K.zeros((nb_runs,), dtype=K.floatx(), name='log_w')
        
        index = K.variable(1, dtype ='int32', name='index')
        
        # prepare the base rate model
        # also do beta = 0 update
        prob_ls, _ = self.dbm.pos_stats(x, 1)
        odd_base_bias = []
        odd_state_ls = []
        eps = 100*np.finfo(np.float32).eps
        
        for pp in prob_ls[1::2]:
            # calculate prob
            p = K.clip(K.mean(pp, axis=0), eps, 1-eps)
                                    
            # this is the base 
            b = K.log(p) - K.log(1-p)
            odd_base_bias += [b]
            
            # this is the sample
            p = K.repeat_elements(K.reshape(p,shape=(1,p.size)), nb_runs, 0) 
            sample = binomial_sample(self.dbm.theano_rng, p)
            odd_state_ls.append(sample)
            
            # this is the update to log_w
            log_w -= K.dot(sample, b)
            
        # need to add in the numbers of neurons in even layers
        for n in self.dbm.layer_size_list[0::2]:
            log_w -= K.variable(n)*K.log(2)

        # these are the centered states
        center_ls = self.dbm.center

        def ais_update(*args):
                        
            # parse the inputs
            args = self.dbm.process_scan_list(args, final=False)
            log_w = args[0]
            index = args[1]
            odd_state_ls = args[2:]
            
            # fill in the needed Nones for even
            state_ls = []
            j=0
            for i in range(len(self.dbm.layers)):
                if i % 2 == 0:
                    state_ls.append(None)
                else:
                    state_ls.append(odd_state_ls[j])
                    j += 1
                    
            beta = betas[index]

            # This updates positive change in log_w and creates even samples
            j = 0
            for i,layer in enumerate(self.dbm.layers):
                if i % 2 == 0:
                    input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                    z = layer.get_input(input_ls, mean_field=True)                                     
                              
                    # Change in log_w from even
                    log_w += K.sum(K.log(1 + K.exp(beta*z)), axis=1)
                    
                    # update probs for even states
                    p = 1.0/(1+K.exp(-beta*z))
                    state_ls[i] = binomial_sample(self.dbm.theano_rng, p)
                    
                elif i % 2 == 1:
                    sample = state_ls[i]
                    bias_base = K.dot(sample, odd_base_bias[j])
                    j = + 1
                    bias_data = K.dot(sample, layer.b)
                    
                    # Change in log_w from odd
                    log_w += (1-beta)*bias_base + beta*bias_data
                    
            # This creates the odd samples
            j = 0
            for i,layer in enumerate(self.dbm.layers):
                
                if i % 2 == 0:
                    pass
                elif i % 2 == 1:
                    input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                    z = layer.get_input(input_ls, mean_field=True)   
                    
                    zz = (1-beta)*odd_base_bias[j] + beta*z
                    j += 1
                    
                    p = 1.0/(1+K.exp(-zz))
                    state_ls[i] = binomial_sample(self.dbm.theano_rng, p)
                    
                    
            # This updates negative change in log_w
            j = 0
            for i,layer in enumerate(self.dbm.layers):
                if i % 2 == 0:
                    input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                    z = layer.get_input(input_ls, mean_field=True)                                     
                              
                    # Change in log_w from even
                    log_w -= K.sum(K.log(1 + K.exp(beta*z)), axis=1)
                                        
                elif i % 2 == 1:
                    sample = state_ls[i]
                    bias_base = K.dot(sample, odd_base_bias[j])
                    j = + 1
                    bias_data = K.dot(sample, layer.b)
                    
                    # Change in log_w from odd
                    log_w -= (1-beta)*bias_base + beta*bias_data              
                    
            # prep for output
            index += 1
            output_ls = [log_w] + [index] + state_ls[1::2]
            
            output_ls = self.dbm.prep_scan_list(output_ls)
            
            return output_ls
        ### end ais scan update

 
        # prepares the input for scan
        output_ls = [log_w] + [index] + odd_state_ls        
        output_ls = self.dbm.prep_scan_list(output_ls)
                                            
        n_steps = int(nb_betas)-2
        scan_out, updates = theano.scan(fn = ais_update, 
                                        outputs_info = output_ls, 
                                        n_steps = n_steps,
                                        name = 'scan_ais')

        K_updates = []
        for k in updates.keys():
            new_tuple = (k, updates[k])
            K_updates.append(new_tuple)
        
        log_w = scan_out[0][-1]
        odd_state_ls = self.dbm.process_scan_list(scan_out[2:], final = True)
        
        state_ls = []
        j=0
        for i in range(len(self.dbm.layers)):
            if i % 2 == 0:
                state_ls.append(None)
            else:
                state_ls.append(odd_state_ls[j])
                j += 1
 
        # This updates final positive change in log_w
        for i,layer in enumerate(self.dbm.layers):
            if i % 2 == 0:
                input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                z = layer.get_input(input_ls, mean_field=True)                                     
                          
                # Change in log_w from even
                log_w += K.sum(K.log(1 + K.exp(z)), axis=1)
                                    
            elif i % 2 == 1:
                sample = state_ls[i]
                bias_data = K.dot(sample, layer.b)
                
                # Change in log_w from odd
                log_w += bias_data

        #####
        # prep for estimate log_z
  
        logZ_base = K.variable(0)
        for n in self.dbm.layer_size_list[0::2]:
            logZ_base += K.variable(n)*K.log(2)
        for b in odd_base_bias:
            logZ_base += K.sum(K.log(1+K.exp(b)))

        return K.function(inputs, [log_w, logZ_base],
                          updates = K_updates, name='ais')

    def compute_log_z(self):
        """
        Compute the log partition function of an (binary-binary) RBM.
        
        This function enumerates a sum with exponentially many terms, and
        should not be used with more than a small, toy model.
        """
        assert len(self.dbm.layer_size_list)==2
        nvis = self.dbm.layer_size_list[0]
        nhid = self.dbm.layer_size_list[1]
                  
        # Pick whether to iterate over visible or hidden states.
        if nvis < nhid:
            width = nvis
            eval_type = 'vis'
        else:
            width = nhid
            eval_type = 'hid'
        
        # Allocate storage for 2**block_bits of the 2**width possible
        # configurations.
        try:
            logz_data_c = np.zeros(
                (2**width, width),
                order='C',
                dtype=K.floatx()
            )
        except MemoryError:
            print('Too big')

        # fill in the first block_bits, which will remain fixed for all
        # 2**width configs
        tensor_10D_idx = np.ndindex(*([2] * width))
        for i, j in enumerate(tensor_10D_idx):
            logz_data_c[i, -width:] = j
        try:
            logz_data = np.array(logz_data_c, order='F', dtype=K.floatx())
        except MemoryError:
            print('Too big')
            
            
        inputs = [K.placeholder(shape=(2**width,width))]
        if self.dbm.uses_learning_phase and not isinstance(K.learning_phase(), int):
            inputs += [K.learning_phase()]
            
        if eval_type == 'vis':
            bias_term = K.dot(inputs[0], self.dbm.layers[0].b)
            z = self.dbm.layers[1].b + K.dot(inputs[0], self.dbm.layers[1].W_ls[0])
            
        elif eval_type == 'hid':
            bias_term = K.dot(inputs[0], self.dbm.layers[1].b)
            z = self.dbm.layers[0].b + K.dot(inputs[0], self.dbm.layers[1].W_ls[0].T)
            
            
        log_term = K.sum(K.log(1+K.exp(z)),axis=1)
        log_prob_vv = bias_term + log_term
            
        fn = K.function(inputs, log_prob_vv)
            
        log_prob = fn([logz_data])        
                                         
        log_z = logsum(log_prob)
        
        return log_z
 
