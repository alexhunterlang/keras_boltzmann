#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os

import numpy as np
import six

import pandas as pd

# Force matplotlib to not use any Xwindows backend.
# Otherwise will get error on linux servers
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import keras
import keras.backend as K

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/keras_boltzmann/src/'
        os.chdir(path_to_script_folder)
        
from ais import ais_estimate_logz
from ais import AIS
        
#%%
class PeriodicSave(keras.callbacks.Callback):
    
    def __init__(self, filepath, epoch_ls):
        super(PeriodicSave, self).__init__()
        self.filepath = filepath
        self.epoch_ls = epoch_ls

    def on_epoch_end(self, epoch, logs=None):        
        # save special epochs
        if epoch in self.epoch_ls:
            filepath = self.filepath.format(epoch=epoch, **logs)
            self.model.save_weights(filepath, overwrite=True)

#%%
class MomentumRateScheduler(keras.callbacks.Callback):
    """Momentum rate scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            momentum rate as output (float).
    """

    def __init__(self, schedule):
        super(MomentumRateScheduler, self).__init__()
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'momentum'):
            raise ValueError('Optimizer must have a "momentum" attribute.')
        momentum = self.schedule(epoch)
        if not isinstance(momentum, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
        K.set_value(self.model.optimizer.momentum, momentum)

#%%
class EpochEnd(keras.callbacks.Callback):
    def __init__(self, outputs_dict, train_data, epoch_ls = []):

        self.outputs = []
        self.outputs_names = []
        for k, v in six.iteritems(outputs_dict):
            self.outputs.append(v)
            self.outputs_names.append(k)
        
        self.train_data = [train_data]
        self.epoch_ls = epoch_ls

        super(EpochEnd, self).__init__()
    
    def on_train_begin(self, logs={}):
        
        if self.model.uses_learning_phase and not isinstance(K.learning_phase(), int):
            inputs = self.model.inputs + [K.learning_phase()]
            self.train_data += [0.0]
        else:
            inputs = self.model.inputs

        outputs = [f(self.model.inputs[0]) for f in self.outputs]
        self.fn = K.function(inputs, outputs)

    def on_train_end(self, logs={}):
        return
 
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):

        if (len(self.epoch_ls)==0) or (epoch in self.epoch_ls):
            
            train_outputs = self.fn(self.train_data)
            valid_outputs = self.fn(self.model.validation_data)
    
            if not isinstance(train_outputs,list):
                train_outputs = [train_outputs]
            if not isinstance(valid_outputs,list):
                valid_outputs = [valid_outputs]
            
            for i in range(len(train_outputs)):
                key = self.outputs_names[i]
                logs[key] = train_outputs[i]
                logs['val_'+key] = valid_outputs[i]
        
        return logs
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return

#%%
class AISCallback(keras.callbacks.Callback):
    def __init__(self, dbm, train_data, nb_runs, nb_betas, epoch_ls = [], name='',
                     exact = False):

        self.nb_runs = nb_runs
        self.nb_betas = nb_betas
        self.epoch_ls = epoch_ls
        self.train_data = [train_data]    
        self.dbm = dbm
        self.exact = exact
        
        if len(name)>0:
            self.name = '_'+name
        else:
            self.name = ''


        super(AISCallback, self).__init__()
    
    def on_train_begin(self, logs={}):
        return
        
    def on_train_end(self, logs={}):
        return
 
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):
        
        if (len(self.epoch_ls)==0) or (epoch in self.epoch_ls):
  
            self.ais = AIS(dbm)
            
            inputs = self.model.inputs
        
        
        self.ais_fn = self.ais.ais_fxn(inputs[0], self.nb_runs, self.nb_betas)
        
        if self.ais.dbm.uses_learning_phase and not isinstance(K.learning_phase(), int):
            self.train_data += [0.0]     
            
            log_w, logZ_base = self.ais_fn(self.train_data)
            logZ_est, logZ_est_up, logZ_est_down = ais_estimate_logz(log_w, logZ_base)
             
            fe = logs.get('free_energy')
            val_fe = logs.get('val_free_energy')

            logs['logz'+self.name] = logZ_est
            logs['logz_high'+self.name] = logZ_est_up
            logs['logz_low'+self.name] = logZ_est_down
            
            logs['prob'+self.name] = -fe - logZ_est
            logs['prob_high'+self.name] = -fe - logZ_est_down
            logs['prob_low'+self.name] = -fe - logZ_est_up
                
            logs['val_prob'+self.name] = -val_fe - logZ_est
            logs['val_prob_high'+self.name] = -val_fe - logZ_est_down
            logs['val_prob_low'+self.name] = -val_fe - logZ_est_up
                
            if self.exact:
                logs['logz_exact'+self.name] = self.dbm.compute_log_z()
                
        else:
            
            for w1 in ['logz', 'prob', 'val_prob']:
                for w2 in ['', '_high', '_low']:
                    logs[w1+w2+self.name] = np.nan
            
            if self.exact:
                logs['logz_exact'+self.name]  = np.nan
            
        return logs
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return

#%%
class SampleCallback(keras.callbacks.Callback):
    def __init__(self, dbm, savefolder, n_chains = 20,
                     n_samples = 10, plot_every = 2000, epoch_ls = []):

        self.dbm = dbm
        self.savefolder = savefolder
        self.n_chains = n_chains
        self.n_samples = n_samples
        self.plot_every = plot_every
        self.epoch_ls = epoch_ls
 
        super(SampleCallback, self).__init__()
    
    def on_train_begin(self, logs={}):
        return
        
    def on_train_end(self, logs={}):
        return
 
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):

        if (len(self.epoch_ls)==0) or (epoch in self.epoch_ls):
            
            filepath = self.savefolder+'/samples_epoch{}.pdf'.format(epoch)
            self.dbm.sample(self.model.validation_data, filepath,
                            self.n_chains, self.n_samples, self.plot_every)
        
        return logs
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return

#%%
class PlotCallback(keras.callbacks.Callback):
    def __init__(self, save_folder, csv_filepath):

        self.save_folder = save_folder
        self.csv_filepath = csv_filepath
 
        super(PlotCallback, self).__init__()
    
    def on_train_begin(self, logs={}):
        return
        
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):
        return
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return
    
    def on_train_end(self, logs={}):
        
        df = pd.read_csv(self.csv_filepath)
        
        data_dict = {}
        for c in df.columns.values:
            data_dict[c] = df[c].values
        
        def plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename):  
            fig = plt.figure()
            for y, color, lbl in zip(y_ls, color_ls, lbl_ls):                
                plt.plot(x,y,color=color,label=lbl)   

            plt.xlabel('Epoch')
            plt.title(title)
            plt.grid(True)
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            fig.savefig(savename, bbox_inches='tight')
            plt.close() 
            
        def plot_errorbars(x, y_ls, color_ls, lbl_ls,
                           x_d, y_d, high_d, low_d, color_d, lbl_d,
                           title, savename):
            
            fig = plt.figure()
            for y, color, lbl in zip(y_ls, color_ls, lbl_ls):
                plt.plot(x,y,color=color,label=lbl)   

            low_err = np.array(y_d)-np.array(low_d)
            high_err = np.array(high_d)-np.array(y_d)
            data_err = np.array([low_err,high_err])
            plt.errorbar(x_d, y_d, yerr=data_err,color=color_d,
                         fmt='o',ls='',label=lbl_d, capsize=10)
            plt.xlim([-5,x[-1]+5]) # so you can see the start/end error bars

            plt.xlabel('Epoch')
            plt.title(title)
            plt.grid(True)
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            fig.savefig(savename, bbox_inches='tight')
            plt.close() 
        
        
        # These plots are similar:
        # Loss, pslike, and recon
        
        x = data_dict['epoch']
        color_ls = ['red', 'blue']
        
        for t in ['loss','pslike','recon']:
            if t in data_dict.keys():
                lbl_ls = [t, 'val_'+t]
                y_ls = [data_dict[lbl_ls[0]], data_dict[lbl_ls[1]]]
                title = t
                savename = self.save_folder+t+'.pdf'
                plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        
        
        # Free energy difference
        if 'fe' in data_dict.keys():
            fe = 'fe'
        else:
            fe = 'free_energy'
        
        if fe in data_dict.keys():
            y_ls = [data_dict[fe]-data_dict['val_'+fe]]
            color_ls = ['black']
            lbl_ls = ['free_energy_diff']
            title = 'Free energy difference, train minus valid'
            savename = self.save_folder+'free_energy.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)


        # logz plot
        lz = [k for k in data_dict.keys() if k.startswith('logz')]
        if len(lz)==3:
            # In this case, assuming just have fast estimate every epoch
            
            t = 'logz'
            lbl_ls = [t, t+'_high', t+'_low']
            y_ls = [data_dict[lbl_ls[0]], data_dict[lbl_ls[1]], data_dict[lbl_ls[2]]]
            color_ls = ['red', 'blue', 'blue']
            title = 'Logz fast. Ranges are +- 3 std.'
            savename = self.save_folder+t+'.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
            
        elif len(lz) > 3:
            # In this case, assuming have fast estimate every epoch and occassional detailed
        
            # only plot median of fast estimate
            y_ls = [data_dict['logz']]
            lbl_ls = ['logz']
            color_ls = ['red']
        
            title = 'Logz fast and detailed estimates. Ranges are +- 3 std.'
            savename = self.save_folder+'logz.pdf'
        
            # prep the error bars of the detailed estimate
        
            name = [n.split('_')[-1] for n in lz if n.startswith('logz_high_')][0]
        
            IS_good = np.logical_not(np.isnan(data_dict['logz_'+name]))
        
            x_d = x[IS_good]
            y_d = data_dict['logz_'+name][IS_good]
            high_d = data_dict['logz_high_'+name][IS_good]
            low_d = data_dict['logz_low_'+name][IS_good]
            color_d = 'black'
            lbl_d = 'logz_'+name
            
            plot_errorbars(x, y_ls, color_ls, lbl_ls,
                           x_d, y_d, high_d, low_d, color_d, lbl_d,
                           title, savename)
            
            
            
        # probability plot
        if len(lz)==3:
            # In this case, assuming just have fast estimate every epoch
                        
            lbl_ls = ['prob',  'val_prob_low', 'val_prob_high']
            y_ls = [data_dict[lbl] for lbl in lbl_ls]
            
            color_ls = ['red', 'blue', 'blue']
            title = 'Probability of valid vs train. Ranges are +- 3 std.'
            savename = self.save_folder+'prob.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
            
        elif len(lz)==6 and (fe in data_dict.keys()):
            # In this case, assuming have fast estimate every epoch and occassional detailed
        
        
            lbl_ls = ['prob',  'val_prob']
            y_ls = [data_dict[lbl] for lbl in lbl_ls]
            
            color_ls = ['red', 'blue']
            title = 'Probability of valid vs train. Ranges are +- 3 std.'
            savename = self.save_folder+'prob.pdf'
        
            # prep the error bars of the detailed estimate
        
            name = [n.split('_')[-1] for n in lz if n.startswith('logz_high_')][0]
        
            IS_good = np.logical_not(np.isnan(data_dict['logz_'+name]))
        
            x_d = x[IS_good]
            y_d = data_dict['val_prob_'+name][IS_good]
            high_d = np.minimum(data_dict['val_prob_high_'+name][IS_good],
                                np.zeros(shape=(y_d.size))) # should cap this out at 0
            low_d = data_dict['val_prob_low_'+name][IS_good]
            
            color_d = 'black'
            lbl_d = 'val_prob_'+name
            
            plot_errorbars(x, y_ls, color_ls, lbl_ls,
                           x_d, y_d, high_d, low_d, color_d, lbl_d,
                           title, savename)
            
        
        return
    