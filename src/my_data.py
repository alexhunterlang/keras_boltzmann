"""
This contains all necessary code to get data into useful objects.

Data is the object for a given dataset. It contains DataSlice objects that
correspond to train, valid, and test. 

There are also a series of functions to actually load datasets.

Code by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3
"""

#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import os
import copy

from keras import backend as K
K.set_image_dim_ordering('th')
from keras.utils.np_utils import to_categorical

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/keras_boltzmann/src/'
        os.chdir(path_to_script_folder)
    
from preprocessing import ImageDataGenerator

#%% parameters
class Data(object):
    def __init__(self,
                 dataset,
                 valid_split = 0.2,
                 index_dict = {},
                 shuffle_trainvalid = False,
                 feature_wise_mean = True,
                 feature_wise_std = False,
                 PCA_transform = False,
                 ZCA_transform = False,
                 prob_norm = False,
                 weight_dict = {},
                 ImageDataGen_dict = {}
                 ):              
        
        if prob_norm:
            num_true = 0
            num_true += feature_wise_mean
            num_true += feature_wise_std
            num_true += PCA_transform
            num_true += ZCA_transform            
            assert num_true == 0
            
        self.dataset = dataset # name of dataset
        
        # Datasets in keras start as train and test only
        # index_dict allows reproducible splits 
        if len(index_dict) == 0:
            index_dict = {'train' : None,
                          'valid' : None,
                          'test' : None,}
        
        # should the train valid split be randomly determined?
        # if index is not none, the shuffle is within these indices
        self.shuffle_trainvalid = shuffle_trainvalid  

        # various preprocessing that can be applied to data
        self.feature_wise_mean = feature_wise_mean
        self.feature_wise_std = feature_wise_std
        self.PCA_transform = PCA_transform
        self.ZCA_transform = ZCA_transform
        
        # keywords for Data Generators
        self.ImageDataGen_dict = ImageDataGen_dict        
        
        # Load up the data  
        datadict = load_data(dataset, valid_split,
                             index_dict, shuffle_trainvalid)        
        
        # Record useful stats
        self.num_ch = datadict['num_ch'] # number of color channels
        self.x = datadict['x'] # x dim of input
        self.y = datadict['y'] # y dim of input
        self.num_input = self.num_ch*self.x*self.y # total dim of input
        self.valid_split = datadict['valid_split']
        
        # these are the common preprocessing steps
        # Saving them so they can be undone
        self.train_feature_mean = datadict['train_feature_mean']
        self.train_feature_std = datadict['train_feature_std']
        self.train_mean = datadict['train_mean']
        self.train_std = datadict['train_std']
        self.PCA = datadict['PCA']
        self.ZCA = datadict['ZCA'] 
        
        if prob_norm:
            eps = np.finfo(np.float32).eps          
            maxmax = -np.inf
            minmin = np.inf
            for d in ['train', 'valid', 'test']:
                temp = np.max(datadict[d+'.data'])
                maxmax = np.maximum(maxmax,temp)
                temp = np.min(datadict[d+'.data'])
                minmin = np.minimum(minmin,temp)
            
            for d in ['train', 'valid', 'test']:
                datadict[d+'.data'] = (datadict[d+'.data']-minmin+eps)/(maxmax-minmin + 2*eps)

            maxmax = -np.inf
            minmin = np.inf
            for d in ['train', 'valid', 'test']:
                temp = np.max(datadict[d+'.data'])
                maxmax = np.maximum(maxmax,temp)
                temp = np.min(datadict[d+'.data'])
                minmin = np.minimum(minmin,temp)
                
            assert np.isclose(minmin, eps)
            assert np.isclose(maxmax, 1-eps)

        else:
            # default is to standardize overall mean and scale if these
            # are not done feature wise
            if feature_wise_mean:
                mean = self.train_feature_mean
            else:
                mean = self.train_mean
            if feature_wise_std:
                std = self.train_feature_std
            else:
                std = self.train_std
            if PCA_transform:
                PCA = self.PCA
            else:
                PCA = None
            if ZCA_transform:
                ZCA = self.ZCA
            else:
                ZCA = None
                
            datadict = apply_data_transforms(datadict, mean = mean,
                                              std = std, ZCA = ZCA, PCA=PCA)
        
        # Make the data slices
        name = 'train'
        try:
            weight = weight_dict[name]
        except KeyError:
            weight = None
        self.train = DataSlice( data = datadict[name+'.data'],
                                lbl = datadict[name+'.lbl'],
                                index_ref = datadict[name+'.index_ref'],                     
                                weight = weight,
                                ImageDataGen_dict = ImageDataGen_dict)
        
        name = 'valid'
        try:
            _ = datadict[name+'.data']
            IS_valid = True
        except KeyError:
            IS_valid = False            
            
        if IS_valid:
            try:
                weight = weight_dict[name]
            except KeyError:
                weight = None
            self.valid = DataSlice( data = datadict[name+'.data'],
                                    lbl = datadict[name+'.lbl'],
                                    index_ref = datadict[name+'.index_ref'],                     
                                    weight = weight,
                                    ImageDataGen_dict = ImageDataGen_dict)        
        
        name = 'test'
        try:
            weight = weight_dict[name]
        except KeyError:
            weight = None
        self.test = DataSlice(  data = datadict[name+'.data'],
                                lbl = datadict[name+'.lbl'],
                                index_ref = datadict[name+'.index_ref'],                     
                                weight = weight,
                                ImageDataGen_dict = ImageDataGen_dict)
        
        self.num_cat = self.train.num_cat # num of label categories  
        
        # sort data by index ref
        self.sort_all()
    
    ################################################################
    @classmethod
    def init_file(cls, filename):
        temp = np.load(filename)
        data_kwargs = {}
        for k in temp.keys():
            if k in ['dataset', 'index_dict',
                     'weight_dict', 'ImageDataGen_dict']:
                data_kwargs[k] = temp[k].item()
            else:
                data_kwargs[k] = temp[k]
        
        return cls(**data_kwargs)
        
    ################################################################
    @classmethod
    def init_sample_data(cls, data_obj, valid_split = None,
                         num_samples = None, bag = True):

        # Assumptions:
        # 1. Make valid first, sample w/out replace equal prob
        #       - weights for valid transfer
        # 2. Train is from leftovers after valid
        #   if bag: sample w/replace, prob is weight but otherwise weight is not passed on
        #   else: all leftovers, pass on weight
        # 3. Test transfered as is

        data_obj = copy.deepcopy(data_obj)
        if valid_split is None:
            valid_split = data_obj.valid_split
        
        data_kwargs =   {
                        'dataset' : data_obj.dataset,
                        'valid_split' : valid_split,
                        'shuffle_trainvalid' : False,
                        'feature_wise_mean' : data_obj.feature_wise_mean,
                        'feature_wise_std' : data_obj.feature_wise_std,
                        'PCA_transform' : data_obj.PCA_transform,
                        'ZCA_transform' : data_obj.ZCA_transform,
                        'ImageDataGen_dict' : data_obj.ImageDataGen_dict,
                        }

        # Number of samples is total size of train
        if num_samples is None:
            num_samples = data_obj.train.num_samples
            
        # first find valid set
        num_valid = int(np.round(num_samples*valid_split))
        index_ref = np.arange(data_obj.train.num_samples)
        index_valid = np.random.choice(index_ref, size=num_valid, replace=False)
                                       
        # now find train set
        num_train = num_samples-num_valid
        if bag:
            prob = copy.deepcopy(data_obj.train.weight)
            prob[index_valid] = 0# shouldn't be picked again                              
            prob = prob/np.sum(prob)
            index_train = np.random.choice(data_obj.train.index_ref,
                                     size=num_train, replace=True, p=prob)
            weight_train = np.ones(num_train)
        else:
            index_train = np.setdiff1d(index_ref, index_valid,
                                       assume_unique=True)
            weight_train = data_obj.train.weight[index_train]
            weight_train = 1.0*num_train*weight_train/np.sum(weight_train)
        
        weight_valid = data_obj.train.weight[index_valid]
        weight_valid = 1.0*num_valid*weight_valid/np.sum(weight_valid)

        data_kwargs['weight_dict'] =    {'train' : weight_train,
                                        'valid' : weight_valid,
                                        'test' : None}

        data_kwargs['index_dict'] = {'train' : index_train,
                                    'valid' : index_valid,
                                    'test' : None}

        return cls(**data_kwargs)        
        
    ################################################################
    def save(self, filename):

        weight_dict =   {
                        'train' : self.train.weight,
                        'test'  : self.test.weight
                        }
        try:
            w = self.valid.weight
        except AttributeError:
            w = None
        weight_dict['valid'] = w                
                
        index_dict =    {
                        'train' : self.train.index_ref,
                        'test'  : self.test.index_ref
                        }
        try:
            index = self.valid.index_ref
        except AttributeError:
            index = None
        index_dict['valid'] = index      

        # These are the kwargs needed to reload a data_obj
        np.savez_compressed(filename,
                            dataset = self.dataset,
                            valid_split = self.valid_split,
                            index_dict = index_dict,
                            shuffle_trainvalid = self.shuffle_trainvalid,
                            feature_wise_mean = self.feature_wise_mean,
                            feature_wise_std = self.feature_wise_std,
                            PCA_transform = self.PCA_transform,
                            ZCA_transform = self.ZCA_transform,
                            weight_dict = weight_dict,
                            ImageDataGen_dict = self.ImageDataGen_dict
                            )    
    
    ################################################################
    def sort_all(self):
        self.train.sort_data()
        self.test.sort_data()
        try:
            self.valid.sort_data()
        except AttributeError:
            pass

#%%      
class DataSlice(object):
    """
    Subset of data, for example train, valid, or test data
    """ 
    
    def __init__(self, data, lbl, index_ref,
                 weight = None, ImageDataGen_dict = {}):
                      
        self.num_samples = data.shape[0]
        self.num_input = np.prod(data.shape[1:])
        self.num_cat = len(set(lbl.flatten()))
        self.ImageDataGen_dict = ImageDataGen_dict        
        
        # have option for weights of each sample, sums to weight.size
        if weight is None:
            weight = np.ones((self.num_samples,), dtype=np.float32)
        else:
            weight = 1.0*weight.size*weight/np.sum(weight)
     
        self.data = data
        self.data_flat = self.data.reshape((self.num_samples, self.num_input))
        self.lbl = lbl.flatten()    
        self.index_ref = index_ref.flatten()
        self.weight = weight.flatten()
        self.one_hot = to_categorical(lbl, self.num_cat)
        
    def shuffle(self):
        input_ls = [self.data, self.lbl, self.index_ref,
                    self.weight, self.one_hot]
        (self.data, self.lbl, self.index_ref,
         self.weight, self.one_hot) = group_shuffle(input_ls)
    
    def sort_data(self):
        input_ls = [self.data, self.lbl, self.index_ref,
                    self.weight, self.one_hot]
        sort_index = np.argsort(self.index_ref)                    
        (self.data, self.lbl, self.index_ref,
         self.weight, self.one_hot) = group_shuffle(input_ls, sort_index)
        
    def get_index_examples(self,num_each):
        # Returns examples of each label        
        index = []        
        for i in range(self.num_cat):
            index += list(np.where(i==self.lbl)[0][0:num_each])
        
        return np.array(index,dtype=np.int32)

    def make_generator(self):
        
        if len(self.ImageDataGen_dict) != 0 :
            gen = ImageDataGenerator(**self.ImageDataGen_dict)  
        else:
            gen = ImageDataGenerator() 
        
        return gen

    def make_flow(self, mbs = 128, include_weight = False):
        
        if include_weight:
            w = copy.deepcopy(self.weight)
        else:
            w = None
            
        gen = self.make_generator()
        
        flow = gen.flow(X = copy.deepcopy(self.data),
                        y = copy.deepcopy(self.one_hot),
                        w = w,
                        batch_size = mbs,
                        shuffle = True)
       
        return flow

#%%
def group_shuffle(input_ls, sort_index = None):
    ''' Shuffles everyone by the same index'''
    
    if sort_index is None:
        sort_index = np.arange(input_ls[0].shape[0], dtype=np.int32)    
        np.random.shuffle(sort_index)    
    
    output_ls = []
    for stuff in input_ls:
        output_ls.append(stuff[sort_index])
    
    return output_ls

#%%
def splitter(train_data, train_lbl, valid_split,
             index_dict, shuffle_trainvalid):
    ''' Splits the training data into train and valid '''
    
    assert (valid_split >= 0.0) and (valid_split < 1.0)

    train_data = copy.deepcopy(train_data).astype(np.float32)
    train_lbl = copy.deepcopy(train_lbl).astype(np.int32)
    index_train = copy.deepcopy(index_dict['train'])
    index_valid = copy.deepcopy(index_dict['valid'])

    if (index_train is not None) and (index_valid is not None):
        index_train = index_train.astype(np.int32)
        index_valid = index_valid.astype(np.int32)
        #if valid_split is not None:
        #    given_split = 1.0*index_valid.size/(index_valid.size+index_train.size)
        #    assert np.isclose(given_split, valid_split)

    elif (index_train is not None) and (index_valid is None):
        index_train = index_train.astype(np.int32)
        num_samples = np.floor(len(index_train)*(1-valid_split)).astype(np.int)        
        index_valid = index_train[num_samples:]  
        index_train = index_train[0:num_samples]

    elif (index_train is None) and (index_valid is None):
             
        index = np.array(range(len(train_lbl)),dtype=np.int32)             
             
        # check that train_data, train_lbl is sorted
        IS_sort = np.all(np.sort(train_lbl)==train_lbl)
        # if sorted, need to randomize for sure
        # otherwise only randomize if shuffle_trainvalid
        if IS_sort or shuffle_trainvalid:            
            np.random.shuffle(index)    
            
        num_samples = np.floor(len(train_lbl)*(1-valid_split)).astype(np.int)
        index_train = index[0:num_samples]
        index_valid = index[num_samples:]
    
    else:
        raise NotImplementedError
   
    # This actually divides up the data
    if index_valid.size > 0:
        valid_data = train_data[index_valid]
        valid_lbl = train_lbl[index_valid]  
    train_data = train_data[index_train]
    train_lbl = train_lbl[index_train] 
    valid_split = 1.0*len(index_valid)/(len(index_valid)+len(index_train))
    assert (valid_split >= 0.0) and (valid_split < 1.0)
    
    output_dict =   {
                    'train.data' : train_data,
                    'train.lbl' : train_lbl,
                    'train.index_ref' : index_train,
                    'valid_split' : valid_split
                    }
    if index_valid.size > 0:
        output_dict['valid.data'] = valid_data
        output_dict['valid.lbl'] = valid_lbl
        output_dict['valid.index_ref'] = index_valid
    
    return output_dict

#%%
def load_data(dataset, valid_split, index_dict, shuffle_trainvalid):
    ''' Loads the keras builtin datasets that I saved to file.
        This allows reproducible indexing.    
    '''

    temp = np.load('../data/'+dataset+'.npz')
    train_data = temp['train_data'] 
    train_lbl = temp['train_lbl'] 
    test_data = temp['test_data'] 
    test_lbl = temp['test_lbl']         

    if len(train_data.shape)==3:
        num_ch = 1
        x = train_data.shape[1]
        y = train_data.shape[2]
        train_data = train_data.reshape((-1,num_ch,x,y))        
        test_data = test_data.reshape((-1,num_ch,x,y))   
    elif len(train_data.shape)==4:
        num_ch = train_data.shape[1]
        x = train_data.shape[2]
        y = train_data.shape[3]
    else:
        raise NotImplementedError

    if index_dict['test'] is not None:
        test_index_ref = index_dict['test']
    else:
        test_index_ref = np.arange(test_lbl.size, dtype=np.float32)

    output_dict = splitter(train_data, train_lbl, valid_split,
                           index_dict, shuffle_trainvalid) 

    datadict =  {
                'test.data' : test_data,
                'test.lbl' : test_lbl,
                'test.index_ref' : test_index_ref,
                'num_ch' : num_ch,
                'x' : x,
                'y' : y,
                }
                
    for k in output_dict.keys():
        datadict[k] = output_dict[k]
        
    keys = ['train_mean', 'train_std',
            'train_feature_mean', 'train_feature_std', 'ZCA', 'PCA']
    for k in keys:
        datadict[k] = temp[k].astype(np.float32)

    return datadict

#%%
def make_data_transforms(train_data):
    ''' Outputs a variety of useful preprocessing transforms '''
        
    # calculate these for reference
    train_feature_mean = np.mean(train_data,axis=0)
    shape = train_feature_mean.shape
    train_feature_mean = train_feature_mean.reshape((1,)+shape).astype(np.float32) 
    train_feature_std = np.std(train_data,axis=0)
    train_feature_std = train_feature_std.reshape((1,)+shape).astype(np.float32)       
        
    train_mean = np.mean(train_data).astype(np.float32)
    train_std = np.std(train_data).astype(np.float32)        
    
    ZCA = make_ZCA_matrix(train_data)
    ZCA = ZCA.astype(np.float32)
    
    ZCA = make_ZCA_matrix(train_data)
    ZCA = ZCA.astype(np.float32)
    
    return train_feature_mean, train_feature_std, train_mean, train_std, ZCA

#%%
def apply_data_transforms(data_dict, mean = 0.0, std = 1.0, ZCA = None):
    ''' Actually applies the transforms '''    
    
    eps = np.finfo(np.float32).eps   
    
    data_keys = [k for k in data_dict.keys() if k.endswith('.data')]
    for d in data_keys:
        data = data_dict[d]
        data = (data-mean)/(std+eps)
        
        if ZCA is not None:
            shape = data.shape
            data = data.reshape((-1, np.prod(shape[1:])))
            data = data.dot(ZCA)
            data = data.reshape(shape)
            
        data_dict[d] = data.astype(np.float32)
    
    return data_dict

#%%
def make_ZCA_matrix(inputs):
    ''' ZCA following keras but actually returns ZCA Matrix '''

    X = copy.deepcopy(inputs)
    eps = np.finfo(np.float32).eps
    
    X = X - np.mean(X, axis=0)
    X = X/(eps + np.std(X, axis=0)) # some people don't do this but I will

    flatX = np.reshape(X, (X.shape[0], np.prod(X.shape[1:])))
    sigma = np.dot(flatX.T, flatX) / flatX.shape[0]
    U, S, V = np.linalg.svd(sigma)
    
    S = np.maximum(S, eps)
    ZCA = np.dot(np.dot(U, np.diag(1. / np.sqrt(S + 1e-4))), U.T)

    return ZCA
    
#%%
# TODO: write code that demonstrates how to go from init dataset to .npz
def make_dataset_file():
    pass