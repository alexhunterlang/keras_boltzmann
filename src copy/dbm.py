# QUESTION:
# 1. if I make dropout and persist non-trainable weights, do they get saved? 
# 2. Do I need to add centering?

# MISC:
# 1. add my own config
# 2. add a tanh option
# 3. restructure persist as a state to use keras code
#       - need to think about how to reset
#       - will still need to take advantage of learning phase

#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
from collections import OrderedDict

import six
from six.moves import zip
import numpy as np

import keras
from keras import backend as K 
from keras.layers.core import Layer, Dense
from keras.metrics import binary_crossentropy
import theano.tensor as T
import theano
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams 

from toposort import toposort_flatten

try:
    import PIL.Image as Image
except ImportError:
    import Image

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/keras_boltzmann/src/'
        os.chdir(path_to_script_folder)

import my_utils
from my_utils import binomial_sample

#%%
def logsum(x):
    # This prevents overflow
    a = np.max(x)
    ls = a + np.log(np.sum(np.exp(x-a)))
    return ls

#%%
def logdiff(x):
    # This prevents overflow
    assert x.shape[0]==2
    a = np.max(x)
    ls = a + np.log(np.diff(np.exp(x-a)))
    return ls.item()

#%%
def prep_topology(topology_dict):
    
    # First check that parity is consistent with current Gibbs sampler
    parity_violation = 0
    for key, value in six.iteritems(topology_dict):
        parity = key % 2
        IS_diff = np.array([parity != (v % 2) for v in value])
        if not np.all(IS_diff):
            parity_violation += 1
    
    if parity_violation:
        raise ValueError('Current DBM Gibbs updates assumes odd / even states are'+
                         ' conditionally independent.')
    
    # this is the order to create layers
    order = toposort_flatten(topology_dict)
    
    # This finds the inputs to each layer
    topology_input_dict = {}
    for  key, value in six.iteritems(topology_dict):
        for i in list(value):
            if i not in topology_input_dict.keys():
                topology_input_dict[i] = [key]
            else:
                topology_input_dict[i] += [key]
    # convert to a set to have same format as topology_dict
    for key, value in six.iteritems(topology_input_dict):
        topology_input_dict[key] = set(value)
    
    return order, topology_input_dict

#%%
class Boltzmann(Dense):
    def __init__(self, output_dim, input_dict = {}, transposed_dict = {},
                 mean_field = True, persist = False, centered = False,
                 dropout_p = 0.0, mbs = None, theano_rng = None, **kwargs):

        self.output_dim = output_dim
        self.mean_field = mean_field
        self.persist = persist
        self.centered = centered
        if self.persist:
            assert mbs is not None
        self.mbs = mbs
        self.p = dropout_p
        if (0. < self.p < 1.) or self.persist:
            self.uses_learning_phase = True
        
        if theano_rng is None:
            theano_rng = RandomStreams(np.random.randint(2 ** 30))
        self.theano_rng = theano_rng
        
        # creates a sorted input_dict
        self.input_dict = OrderedDict()
        if len(input_dict)>0:
            for k in np.sort(input_dict.keys()):
                self.input_dict[k] = input_dict[k]
            
        # creates a sorted transposed_dict
        self.transposed_dict = OrderedDict()
        if len(transposed_dict)>0:
            for k in np.sort(transposed_dict.keys()):
                self.transposed_dict[k] = transposed_dict[k]
            
        # assign layer properties
        len_in = len(self.input_dict)
        len_tran = len(self.transposed_dict)
        if len_in > 0 and len_tran > 0:
            self.layer_type = 'middle'
            self.direction = 'both'
            # need to compensate for reduced inputs
            self.z_up_adj = 1.0*(len_in+len_tran)/len_in
            self.z_down_adj = 1.0*(len_in+len_tran)/len_tran
        elif len_in > 0 and len_tran == 0:
            self.layer_type = 'output'
            self.direction = 'up'
        elif len_in==0 and len_tran > 0:
            self.layer_type = 'input'
            self.direction = 'down'
        else:
            raise NotImplementedError
     
        super(Boltzmann, self).__init__(output_dim, **kwargs) 


    def build(self):
        self.build_W()
        self.build_non_W()
        
    def build_non_W(self):
        self.my_index = int(self.name[1:])
        
        self.transposed_index = []
        self.W_T_ls = []
        for key, layer in six.iteritems(self.transposed_dict):
            name = 'h{}-{}_W'.format(key, self.name)
            self.W_T_ls.append(layer.get_named_weights(name).T)
            self.transposed_index.append(key)

        if self.bias:
            self.b = self.add_weight((self.output_dim,),
                                     initializer='zero',
                                     name='{}_b'.format(self.name),
                                     regularizer=self.b_regularizer,
                                     constraint=self.b_constraint)
        else:
            self.b = None
            
        if self.centered:
            self.c = self.add_weight((self.output_dim,),
                                     initializer='zero',
                                     name='{}_c'.format(self.name),
                                     regularizer = None,
                                     constraint = None,
                                     trainable = False)
        else:
            self.c = None

        self.make_dropout_mask()

        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True
        
    def build_W(self):

        self.input_index = []
        self.W_ls = []
        for key, input_dim in six.iteritems(self.input_dict):
            W = self.add_weight((input_dim, self.output_dim),
                                initializer=self.init,
                                name='{}-h{}_W'.format(self.name, key),
                                regularizer=self.W_regularizer,
                                constraint=self.W_constraint)
            self.W_ls.append(W)
            self.input_index.append(key)
        
    def check_updates(self, tensor, x):
        
        # This implements a valid update for once per K.function call
        current_updates = self.get_updates_for(x)
        IS_updated = False
        for up in current_updates:
            if tensor in up:
                IS_updated = True
        
        return IS_updated
 
    def make_dropout_mask(self, x = None):
        if self.p < 0. or self.p >= 1:
            raise ValueError('Dropout level must be in interval [0, 1[.')
        
        if self.p > 0:
            prob = (1-self.p)*K.ones((self.output_dim,))
            dropout_mask = binomial_sample(self.theano_rng, prob)    
            dropout_mask.name = self.name+'_dropout'
            
            if x is not None:
                IS_updated = self.check_updates(dropout_mask, x)
                        
                if not IS_updated:
                    self.add_update((self.dropout_mask, dropout_mask), x) # NOTE: is this actually for x
            else:
                # This is okay if not updated per K.function
                self.dropout_mask = dropout_mask
    
    def apply_dropout(self, x):
        
        if 0. < self.p < 1.:
            def dropped_inputs():
                return (x*self.dropout_mask)/(1.0-self.p)
            x = K.in_train_phase(dropped_inputs, lambda: x)
            self.make_dropout_mask(x)
        
        return x
 
    def make_persist_chain(self, p = None, x = None):
        
        if self.persist:
            if p is None:
                p = 0.5*K.ones((self.mbs, self.output_dim), dtype=K.floatx())
                persist_chain = K.variable(p, dtype=K.floatx())
            else:
                persist_chain = p
            
            persist_chain.name = self.name+'_persist'                                     
                                                 
            if x is not None:
                IS_updated = self.check_updates(persist_chain, x)
                        
                if not IS_updated:
                    self.add_update((self.persist_chain, persist_chain), x) # NOTE: is this actually for x
            else:
                # This is okay if not updated per K.function
                self.persist_chain = persist_chain
    
    def apply_center(self, x):
        if self.centered:
            x -= self.c   
        return x
    
    def call(self, x, make=None):
        raise NotImplementedError

    def get_named_weights(self, name):
        """Returns a reference to a specificly named weight
        """
        for w in self.weights:
           if w.name==name:
               return w
        
    def needed_inputs(self, mean_field=True, direction = None):
        
        index = []
        if direction is None:
            direction = self.direction
        
        if direction in ['both','up']:
            index += self.input_index
        if not mean_field:
            index += [self.my_index]
        if direction in ['both','down']:
            index += self.transposed_index
            
        return index
    
    def prep_input(self, full_input_ls, center_ls=None,
                           mean_field=True, direction=None):
        if direction is None:
            direction = self.direction
            
        if center_ls is None:
            center_ls = len(full_input_ls)*[None]
            
        index = self.needed_inputs(mean_field, direction)
        
        input_ls = []
        for j in index:
            if center_ls[j] is None:
                input_ls.append(full_input_ls[j])
            else:
                input_ls.append(full_input_ls[j] - center_ls[j])
        
        return input_ls

    def get_input(self, input_ls, mean_field=True, direction=None):
        
        if direction is None:
            direction = self.direction
        
        # Controls which interactions to include    
        IS_up = True
        IS_down = True   
        if direction=='up' or self.direction=='up':
            IS_down = False            
        elif direction=='down' or self.direction=='down':
            IS_up = False
        
        input_self = None
        start = 0
        stop = 0
        current_W_ls = []
        current_input = []
        if IS_up:
            stop = len(self.W_ls)
            current_input += input_ls[start:stop]
            current_W_ls += self.W_ls
        if not mean_field:
            input_self = input_ls[stop]
            stop += 1
        if IS_down:
            current_input += input_ls[stop:]
            current_W_ls += self.W_T_ls
           
            
        # Calculate activation input
        z = self.b
        for data, W in zip(current_input, current_W_ls):
            z += K.dot(data, W)
            if not mean_field:
                z += -0.5*K.dot(data-data**2, W**2)*(input_self-0.5)
                
        # need to compensate for reduced input        
        if (self.layer_type=='middle') and (direction == 'up'):
            z *= self.z_up_adj  
        elif (self.layer_type=='middle') and (direction == 'down'):
            z *= self.z_down_adj
            
        return z

    def get_output(self, input_ls, mean_field=True, direction=None):
        
        z = self.get_input(input_ls, mean_field, direction)
        prob = self.apply_dropout(self.activation(z))
            
        return prob

#%%
class DBM(Layer):
    """
    Deep Restricted Boltzmann Machine (DBM).
    """

    def __init__(self, layer_size_list, topology_dict = None,
                 mean_field = True, persist = False, centered = False,
                 dropout_prob_list = None,
                 mbs = None, weights_dict = None, theano_rng = None,
                 init_mbs = None, vis_mean = None, **kwargs):

        kwargs = self.init_general(layer_size_list, topology_dict,
                                   mean_field, persist, centered,
                                   dropout_prob_list, mbs,
                                   theano_rng, init_mbs, vis_mean, **kwargs)
        
        self.init_layers(weights_dict, **kwargs)
        
        self.init_values(weights_dict, init_mbs, vis_mean)


    def init_general(self, layer_size_list, topology_dict,
                 mean_field, persist, centered, dropout_prob_list,
                 mbs, theano_rng, init_mbs, vis_mean, **kwargs):

        self.layer_size_list = layer_size_list
        self.topology_dict = topology_dict
        self.mean_field = mean_field
        self.persist = persist
        self.centered = centered
        if dropout_prob_list is None:
            dropout_prob_list = [0.0]*len(layer_size_list)
        self.dropout_prob_list = dropout_prob_list
        assert len(self.layer_size_list) == len(self.dropout_prob_list)
        self.mbs = mbs
        if self.persist:
            assert self.mbs is not None
            assert init_mbs is not None
        if self.centered:
            assert vis_mean is not None
        self.input_dim = layer_size_list[0]

        kwargs['activation'] = 'sigmoid'
        kwargs['bias'] = True
        
        if theano_rng is None:
            theano_rng = RandomStreams(np.random.randint(2 ** 30))
        self.theano_rng = theano_rng
        
        return kwargs
            

    def init_layers(self, weights_dict, **kwargs):   
        # initialize the layers
        self.layers_indexs = {}
        self.layers_depths = {}
        order, topology_input_dict = prep_topology(self.topology_dict)
        self.topology_input_dict = topology_input_dict
        depth = 0
        for i in order:
            name = 'h'+str(i)
            if weights_dict is not None:
                weights = self.prep_weights_layer(weights_dict, name)
            else:
                weights = None 

            input_dict = {}
            if i in self.topology_input_dict.keys():
                for j in list(self.topology_input_dict[i]):
                    input_dict[j] = self.layer_size_list[j]

            transposed_dict = {}
            if i in self.topology_dict.keys():
                for j in self.topology_dict[i]:
                    transposed_dict[j] = self.layers_indexs[j]
                
            # Only going to center to visible layer since higher ones should 
            # basically be zero and are not worth the effort to compute
            if i == 0:
                centered = self.centered
            else:
                centered = False
            layer = Boltzmann(self.layer_size_list[i],
                              input_dict = input_dict,
                              transposed_dict = transposed_dict,
                              mean_field = self.mean_field,
                              persist = self.persist,
                              centered = centered,
                              dropout_p = self.dropout_prob_list[i],
                              mbs = self.mbs,
                              name=name,
                              weights = weights,
                              theano_rng = self.theano_rng,
                              **kwargs)
            
            layer.build()
            
            self.layers_indexs[i] = layer
            self.layers_depths[depth] = layer
            depth += 1
            
    def init_values(self, weights_dict, init_mbs, vis_mean):
            
        # put layers in order from low to high
        self.layers = []
        for s in np.sort(self.layers_indexs.keys()):
            self.layers.append(self.layers_indexs[s])
            
        uses_learning_phase = 0
        for lay in self.layers:
            uses_learning_phase += lay.uses_learning_phase
            
        self.uses_learning_phase = (uses_learning_phase > 0)
                                         
        self.updates = []
        
        # The first initialization was just to get all the nececessary weights made
        # Now, need to initialize to reasonable values
        if (weights_dict is None) and (vis_mean is not None):
            # Just set initial layer to MLE of data
            b = np.log(vis_mean/(1.0-vis_mean)).astype(K.floatx())
            K.set_value(self.layers[0].b, b)
             
        if self.persist:

            input = K.placeholder(shape=(self.mbs, self.layer_size_list[0]))
            fxn_ls = [input]
            init_mbs_ls = [init_mbs]
            if self.uses_learning_phase and not isinstance(K.learning_phase(), int):
                fxn_ls += [K.learning_phase()]
                init_mbs_ls += [0.0]
            fn = K.function(fxn_ls, self.propup(input))
            prob_ls = fn(init_mbs_ls)
        
            for i,lay in enumerate(self.layers):
                p = K.variable(prob_ls[i], dtype=K.floatx())
                lay.make_persist_chain(p=p, x=None)
                
        if self.centered:
            layer = self.layers[0]
            c_new = K.sigmoid(layer.b).eval()
            K.set_value(layer.c, c_new)

 
    def prep_weights_layer(self, weights_dict, name):
        d = weights_dict
   
        keys = [k for k in d.keys() if k.startswith(name)]
        assert len(keys)>0
        
        weight_keys = [k for k in keys if k.endswith('_W')]
        input_layers = np.sort([int(w.split('-')[1].split('_')[0][1:]) for w in weight_keys])
                  
        output = []
        for i in input_layers:
            W_name = name+'-h'+str(i)+'_W'
            W = d[W_name].astype(np.float32)
            output.append(W)
        
        try:
            b = d[name+'_b'].astype(np.float32)
            output.append(b)
        except KeyError:
            pass
        
        return output
    
    def __call__(self,x,mask=None):
        return NotImplementedError
              
    def call(self, x, mask=None):
        return NotImplementedError
        
    def get_output_shape_for(self, input_shape):
        return NotImplementedError

    def make_persist_chain(self, prob_ls, x):
        if self.persist:
            for layer, p in zip(self.layers, prob_ls):
                layer.make_persist_chain(p, x)
                
    def make_center(self, prob_ls, x):
        if self.centered:
            input_ls = self.apply_center(prob_ls)
            for layer in self.layers:
                layer.centering_update(input_ls, x)

    @property
    def persist_chain(self):
        persist_chain = []
        for layer in self.layers:
            persist_chain.append(layer.persist_chain)
        return persist_chain
    
    @property
    def center(self):
        center = []
        for layer in self.layers:
            center.append(layer.c)
        return center

    def apply_center(self, prob_ls):
        for i, layer in enumerate(self.layers):
            prob_ls[i] = layer.apply_center(prob_ls[i])
            
        return prob_ls

    def prop(self, x, direction='up'):
        
        if direction == 'up':
            start = 0
            layers_ls = self.layers[1:]
        elif direction == 'down':
            start = -1
            layers_ls = self.layers[::-1][1:]
            
        x = self.layers[start].apply_dropout(x)
        
        prob_ls = [x]+[None]*(len(self.layers)-1)
        center_ls = self.center
        
        for i, layer in enumerate(layers_ls):
            input_ls = layer.prep_input(prob_ls, center_ls = center_ls,
                                        direction = direction, mean_field = True)      
            prob_ls[i+1] = layer.get_output(input_ls, direction = direction, mean_field = True)

        if direction == 'down':
            prob_ls = prob_ls[::-1]
            
        return prob_ls

    def propup(self,x):
        return self.prop(x, direction = 'up')

    def propdown(self,x):
        return self.prop(x, direction = 'down')

    def free_energy(self, x):
        ''' Function to compute the free energy of a visible sample '''
        
        prob_ls = self.propup(x)

        return self.free_energy_given_h(prob_ls)        

    def free_energy_given_h(self, prob_ls):
        """ Function for free energy given visible sample and 
        activations of hidden layers        
        """
 
        z_odd_ls = []
        if self.centered:
            center_ls = self.center
        else:
            center_ls = None
        for layer in self.layers[1::2]:
            input_ls = layer.prep_input(prob_ls, center_ls,
                                        direction = 'both', mean_field = True)            
            z = layer.get_input(input_ls, direction = 'both', mean_field = True)
            z_odd_ls.append(z)

        z = K.concatenate(z_odd_ls, axis=1)
        fe = -K.sum(K.log(1 + K.exp(z)), axis=1)

        for p, layer in zip(prob_ls[0::2], self.layers[0::2]):
            fe -= K.dot(layer.apply_center(p), layer.b)
    
        return fe

    def parity_update(self, prob_ls, sample_ls, start,
                              IS_prob_input = True, hold_constant = []):
        ''' Updates either even or odd layers of synapses'''

        index = list(range(start,len(prob_ls),2))
        index = [i for i in index if i not in hold_constant]
        if len(index)==0:
            prob_out = prob_ls
            sample_out = sample_ls
        else:
            prob_out = [None]*len(prob_ls)
            sample_out = [None]*len(sample_ls)
                    
            for i, s in enumerate(sample_ls):
                if s is None:
                    p = prob_ls[i]
                    sample_ls[i] = binomial_sample(self.theano_rng, p)
            
            if IS_prob_input:
                input_ls = prob_ls
            else:
                input_ls = sample_ls
                
            center_ls = self.center
            for i in index:
                layer = self.layers[i]
                layer_input_ls = layer.prep_input(input_ls, center_ls, direction = 'both', mean_field = self.mean_field)
                p = layer.get_output(layer_input_ls, direction = 'both', mean_field = self.mean_field) 
                prob_out[i] = p                    
                sample_ls[i] = binomial_sample(self.theano_rng, p)
                
            # pass out the unchanged prob / samples
            for i in range(len(prob_out)):
                if prob_out[i] is None:
                    prob_out[i] = prob_ls[i]
                if sample_out[i] is None:
                    sample_out[i] = sample_ls[i]
        
        
        return prob_out, sample_out
 
    def gibbs_p2p(self, output_ls, start='even', hold_constant = []):
        ''' This function implements one step of Gibbs sampling,
            starting from the a given parity and returning to that parity'''

        if start=='odd':
            index1 = 0
            index2 = 1
            IS_prob_input = False
        elif start=='even':
            index1 = 1
            index2 = 0
            IS_prob_input = True
        else:
            raise NotImplementedError

        if len(output_ls)==len(self.layers):
            prob_ls = output_ls
            sample_ls = [None]*len(prob_ls)
        else:
            mid = int(len(output_ls)/2)
            prob_ls = output_ls[0:mid]
            sample_ls = output_ls[mid:]

        # Update 1
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, index1, 
                                                IS_prob_input = IS_prob_input,
                                                hold_constant = hold_constant)
        # update 2
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, index2,
                                                IS_prob_input = IS_prob_input,
                                                hold_constant = hold_constant)

        output_ls = prob_ls+sample_ls 

        return output_ls 

    def gibbs_p2p_scan(self, start='even', hold_constant = []):
        ''' This function implements one step of Gibbs sampling,
            starting from the a given parity and returning to that parity
            in a manner that is friendly to theano.scan
            '''
        
        def gibbs(*args):
                             
            output_ls = list(args) 
            output_ls = self.process_scan_list(output_ls)
            output_ls = self.gibbs_p2p(output_ls, start=start, hold_constant = hold_constant)
            output_ls = self.prep_scan_list(output_ls)
                                    
            return output_ls
        
        return gibbs

    def prep_scan_list(self, output_ls):
        """ Preps inputs for scan """
        
        if self.uses_learning_phase and not isinstance(K.learning_phase(), int):
            output_ls += [K.learning_phase()]
            
        return output_ls
    
    def process_scan_list(self, output_ls, final = False):
        """ Processes outputs from scan """
        
        if self.uses_learning_phase and not isinstance(K.learning_phase(), int):
            output_ls = output_ls[0:-1]
        
        if final:
            output_ls = [out[-1] for out in output_ls]
        
        return output_ls
        

    def pos_stats(self, x, nb_gibbs_steps):
       
        if len(self.layers) == 2:
            nb_gibbs_steps = 1 # rbm only needs a single step
        
        # use propup to get initial mf activations
        prob_data = self.propup(x)
        sample_data = [binomial_sample(self.theano_rng, p) for p in prob_data]
        
        output_ls = prob_data+sample_data    
        output_ls = self.prep_scan_list(output_ls)
        
        gibbs = self.gibbs_p2p_scan('even', hold_constant = [0])
        
        # Note: this could be turned into a scan,
        # but I don't think it would lead to much change in runtime
        for i in range(nb_gibbs_steps):
            output_ls = gibbs(*output_ls)
 
        output_ls = self.process_scan_list(output_ls, final=False)
        mid = int(len(output_ls)/2)
        prob_data = output_ls[0:mid]
        sample_data = output_ls[mid:]
        
        return prob_data, sample_data

    def neg_stats(self, prob_data, sample_data, nb_gibbs_steps):
         
        if self.persist:
            prob_model = []
            sample_model = []
            for pc, pd, sd in zip(self.persist_chain, prob_data, sample_data):
                p = K.in_train_phase(pc, pd)
                s = K.in_train_phase(binomial_sample(self.theano_rng, p),sd)
                prob_model.append(s)
                sample_model.append(s)
        else:
            prob_model = prob_data
            sample_model = sample_data

        output_ls = prob_model+sample_model
        output_ls = self.prep_scan_list(output_ls)
 
        gibbs = self.gibbs_p2p_scan('odd')
        
        # Note: this could be turned into a scan,
        # but I don't think it would lead to much change in runtime
        for i in range(nb_gibbs_steps):
            output_ls = gibbs(*output_ls)
       
        output_ls = self.process_scan_list(output_ls, final=False)
        mid = int(len(output_ls)/2)
        prob_model = output_ls[0:mid]
        sample_model = output_ls[mid:]

        if K.in_train_phase(1, 0):
            self.make_persist_chain(prob_model, prob_data[0])

        # Ref [2] recommends to always use probs for update statistics        
        return prob_model, sample_model

    def contrastive_divergence_loss(self, pos_steps = 1, neg_steps = 5):
        """
        Compute contrastive divergence loss with k steps of Gibbs sampling (CD-k).

        Result is a Theano expression with the form loss = f(x).
        """
        def cd(x):
            prob_data, sample_data = self.pos_stats(x, pos_steps)
            prob_model, sample_model = self.neg_stats(prob_data, sample_data, neg_steps)
            
            model = [sample_model[0]]+prob_model[1:]
            
            cd = K.mean(self.free_energy_given_h(prob_data), axis=-1) - K.mean(self.free_energy_given_h(model), axis=-1)            
            
            prob_data = [prob_data[0]]+[K.stop_gradient(p) for p in prob_data[1:]]
            prob_model = [K.stop_gradient(p) for p in prob_model]
            
            return cd

        return cd

    def reconstruction_loss(self, pos_steps = 1, neg_steps = 5):
        """
        Compute binary cross-entropy between the binary input data and the reconstruction generated by the model.

        Result is a Theano expression with the form loss = f(x).

        Useful as a rough indication of training progress (see Hinton2010).
        Summed over feature dimensions, mean over samples.
        """

        def recon(x):
            prob_data, sample_data = self.pos_stats(x, pos_steps)
            prob_model, sample_model = self.neg_stats(prob_data, sample_data, neg_steps)
            x = self.layers[0].apply_dropout(x)
            cross_entropy_loss = binary_crossentropy(x, sample_model[0])

            prob_data = [prob_data[0]]+[K.stop_gradient(p) for p in prob_data[1:]]
            prob_model = [K.stop_gradient(p) for p in prob_model]
            
            return cross_entropy_loss
        
        return recon

    def pseudo_likelihood_loss(self):
        """
        Pseudo-likelihood. Slow so do not use on every batch.   
        """
        # Could make this into a scan to speed up
        
        def pseudo_likelihood(x):
            
            cost = K.variable(0.0,dtype=K.floatx(), name='ps_cost')
            free_energy = self.free_energy(x)
                        
            for i in range(self.input_dim):
                x_flip = T.set_subtensor(x[:,i], 1-x[:,i])
                free_energy_flip = self.free_energy(x_flip)
                cost += K.mean(K.log(K.sigmoid(free_energy_flip-free_energy)), axis=-1)
            
            return cost

        return pseudo_likelihood

    def free_energy_loss(self):
        """
        Puts free energy in format needed by keras metrics
        """
        
        def free_energy(x):
            return K.mean(self.free_energy(x), axis=-1)
        
        return free_energy


    def ais_fxn(self, x, nb_runs = 100, nb_betas = 1e3):
        """
        Annealed importance sampling
        """
        
        inputs = [x]
        
        if self.uses_learning_phase and not isinstance(K.learning_phase(), int):
            inputs += [K.learning_phase()]
        
        # this is similar to the proportions used by Ruslan
        num_low = int(0.035*nb_betas)
        num_mid = int(0.275*nb_betas)
        num_high = nb_betas - num_low - num_mid
        betas = np.hstack((np.linspace(0, 0.5, num_low),
                           np.linspace(0.5, 0.9, num_mid),
                           np.linspace(0.9, 1, num_high)))
        
        betas = K.variable(betas, dtype = K.floatx(), name='betas')

        log_w = K.zeros((nb_runs,), dtype=K.floatx(), name='log_w')
        
        index = K.variable(1, dtype ='int32', name='index')
        
        # prepare the base rate model
        # also do beta = 0 update
        prob_ls, _ = self.pos_stats(x, 1)
        odd_base_bias = []
        odd_state_ls = []
        eps = 100*np.finfo(np.float32).eps
        
        for pp in prob_ls[1::2]:
            # calculate prob
            p = K.clip(K.mean(pp, axis=0), eps, 1-eps)
                                    
            # this is the base 
            b = K.log(p) - K.log(1-p)
            odd_base_bias += [b]
            
            # this is the sample
            p = K.repeat_elements(K.reshape(p,shape=(1,p.size)), nb_runs, 0) 
            sample = binomial_sample(self.theano_rng, p)
            odd_state_ls.append(sample)
            
            # this is the update to log_w
            log_w -= K.dot(sample, b)
            
        # need to add in the numbers of neurons in even layers
        for n in self.layer_size_list[0::2]:
            log_w -= K.variable(n)*K.log(2)

        # these are the centered states
        center_ls = self.center

        def ais_update(*args):
                        
            # parse the inputs
            args = self.process_scan_list(args, final=False)
            log_w = args[0]
            index = args[1]
            odd_state_ls = args[2:]
            
            # fill in the needed Nones for even
            state_ls = []
            j=0
            for i in range(len(self.layers)):
                if i % 2 == 0:
                    state_ls.append(None)
                else:
                    state_ls.append(odd_state_ls[j])
                    j += 1
                    
            beta = betas[index]

            # This updates positive change in log_w and creates even samples
            j = 0
            for i,layer in enumerate(self.layers):
                if i % 2 == 0:
                    input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                    z = layer.get_input(input_ls, mean_field=True)                                     
                              
                    # Change in log_w from even
                    if i==0 and self.centered:
                        c = layer.c
                        log_w += K.sum(K.log(K.exp(-beta*z*c) + K.exp(beta*z*(1-c))), axis=1)
                    else:
                        log_w += K.sum(K.log(1 + K.exp(beta*z)), axis=1)
                    
                    # update probs for even states
                    p = 1.0/(1+K.exp(-beta*z))
                    state_ls[i] = binomial_sample(self.theano_rng, p)
                    
                elif i % 2 == 1:
                    sample = state_ls[i]
                    bias_base = K.dot(sample, odd_base_bias[j])
                    j = + 1
                    bias_data = K.dot(sample, layer.b)
                    
                    # Change in log_w from odd
                    log_w += (1-beta)*bias_base + beta*bias_data
                    
            # This creates the odd samples
            j = 0
            for i,layer in enumerate(self.layers):
                
                if i % 2 == 0:
                    pass
                elif i % 2 == 1:
                    input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                    z = layer.get_input(input_ls, mean_field=True)   
                    
                    zz = (1-beta)*odd_base_bias[j] + beta*z
                    j += 1
                    
                    p = 1.0/(1+K.exp(-zz))
                    state_ls[i] = binomial_sample(self.theano_rng, p)
                    
                    
            # This updates negative change in log_w
            j = 0
            for i,layer in enumerate(self.layers):
                if i % 2 == 0:
                    input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                    z = layer.get_input(input_ls, mean_field=True)                                     
                              
                    # Change in log_w from even
                    if i==0 and self.centered:
                        c = layer.c
                        log_w -= K.sum(K.log(K.exp(-beta*z*c) + K.exp(beta*z*(1-c))), axis=1)
                    else:
                        log_w -= K.sum(K.log(1 + K.exp(beta*z)), axis=1)
                                        
                elif i % 2 == 1:
                    sample = state_ls[i]
                    bias_base = K.dot(sample, odd_base_bias[j])
                    j = + 1
                    bias_data = K.dot(sample, layer.b)
                    
                    # Change in log_w from odd
                    log_w -= (1-beta)*bias_base + beta*bias_data              
                    
            # prep for output
            index += 1
            output_ls = [log_w] + [index] + state_ls[1::2]
            
            output_ls = self.prep_scan_list(output_ls)
            
            return output_ls
        ### end ais scan update

 
        # prepares the input for scan
        output_ls = [log_w] + [index] + odd_state_ls        
        output_ls = self.prep_scan_list(output_ls)
                                            
        n_steps = int(nb_betas)-2
        scan_out, updates = theano.scan(fn = ais_update, 
                                        outputs_info = output_ls, 
                                        n_steps = n_steps,
                                        name = 'scan_ais')

        K_updates = []
        for k in updates.keys():
            new_tuple = (k, updates[k])
            K_updates.append(new_tuple)
        
        log_w = scan_out[0][-1]
        odd_state_ls = self.process_scan_list(scan_out[2:], final = True)
        
        state_ls = []
        j=0
        for i in range(len(self.layers)):
            if i % 2 == 0:
                state_ls.append(None)
            else:
                state_ls.append(odd_state_ls[j])
                j += 1
 
        # This updates final positive change in log_w
        for i,layer in enumerate(self.layers):
            if i % 2 == 0:
                input_ls = layer.prep_input(state_ls, center_ls, mean_field=True)
                z = layer.get_input(input_ls, mean_field=True)                                     
                          
                # Change in log_w from even
                if i==0 and self.centered:
                    c = layer.c
                    log_w += K.sum(K.log(K.exp(-z*c) + K.exp(z*(1-c))), axis=1)
                else:
                    log_w += K.sum(K.log(1 + K.exp(z)), axis=1)
                                    
            elif i % 2 == 1:
                sample = state_ls[i]
                bias_data = K.dot(sample, layer.b)
                
                # Change in log_w from odd
                log_w += bias_data

        #####
        # prep for estimate log_z
  
        logZ_base = K.variable(0)
        for n in self.layer_size_list[0::2]:
            logZ_base += K.variable(n)*K.log(2)
        for b in odd_base_bias:
            logZ_base += K.sum(K.log(1+K.exp(b)))

        return K.function(inputs, [log_w, logZ_base],
                          updates = K_updates, name='ais')

    def compute_log_z(self):
        """
        Compute the log partition function of an (binary-binary) RBM.
        
        This function enumerates a sum with exponentially many terms, and
        should not be used with more than a small, toy model.
        """
        assert len(self.layer_size_list)==2
        nvis = self.layer_size_list[0]
        nhid = self.layer_size_list[1]
                  
        # Pick whether to iterate over visible or hidden states.
        if nvis < nhid:
            width = nvis
            eval_type = 'vis'
        else:
            width = nhid
            eval_type = 'hid'
        
        # Allocate storage for 2**block_bits of the 2**width possible
        # configurations.
        try:
            logz_data_c = np.zeros(
                (2**width, width),
                order='C',
                dtype=K.floatx()
            )
        except MemoryError:
            print('Too big')

        # fill in the first block_bits, which will remain fixed for all
        # 2**width configs
        tensor_10D_idx = np.ndindex(*([2] * width))
        for i, j in enumerate(tensor_10D_idx):
            logz_data_c[i, -width:] = j
        try:
            logz_data = np.array(logz_data_c, order='F', dtype=K.floatx())
        except MemoryError:
            print('Too big')
            
            
        inputs = [K.placeholder(shape=(2**width,width))]
        if self.uses_learning_phase and not isinstance(K.learning_phase(), int):
            inputs += [K.learning_phase()]
            
        if eval_type == 'vis':
            inputs_0 = inputs[0]
            if self.centered:
                inputs_0 -= self.layers[0].c
            bias_term = K.dot(inputs_0, self.layers[0].b)
            z = self.layers[1].b + K.dot(inputs_0, self.layers[1].W_ls[0])
            
        elif eval_type == 'hid':
            bias_term = K.dot(inputs[0], self.layers[1].b)
            z = self.layers[0].b + K.dot(inputs[0], self.layers[1].W_ls[0].T)
            
        if self.centered and eval_type=='hid':
            c = self.layers[0].c
            log_term = K.sum(K.log(K.exp(-z*c)+K.exp(z*(1-c))),axis=1)
        else:
            log_term = K.sum(K.log(1+K.exp(z)),axis=1)

        log_prob_vv = bias_term + log_term
            
        fn = K.function(inputs, log_prob_vv)
            
        log_prob = fn([logz_data])        
                                         
        log_z = logsum(log_prob)
        
        return log_z
 
    def sample(self, data, filepath, n_chains = 20, n_samples = 10, plot_every = 2000):
        
        n_data = data[0].shape[0]
        n_features = data[0].shape[1]
        
        assert n_data >= n_chains
                         
        index = np.random.choice(np.arange(n_data), size = n_chains, replace = False).astype('int')
        
        init_v = data[0][index]
        
        init_v_var = K.variable(init_v, dtype=K.floatx(), name='init_v')
        
        chain_ls = self.propup(init_v_var)
        
        sample_ls = len(chain_ls)*[None]
        
        output_ls = chain_ls+sample_ls   
        
        output_ls = self.prep_scan_list(output_ls)
        
        gibbs = self.gibbs_p2p_scan(start='even')
        
        scan_output, updates = theano.scan(gibbs, 
                                           outputs_info = output_ls, 
                                           n_steps = plot_every,
                                           name = 'scan_sample')
        
        output_ls = self.process_scan_list(scan_output)
        mid = int(len(output_ls)/2)
        prob_out = output_ls[0:mid]
    
        prob_v = prob_out[0]
    
        # construct the function that implements our persistent chain.
        inputs = self.prep_scan_list([])
        sample_fxn = theano.function(inputs, prob_v[-1],
                                     updates=updates, name='sample_fxn')
        
        # create a space to store the image for plotting ( we need to leave
        # room for the tile_spacing as well)
        ts = 1 # tile spacing
        x = int(np.sqrt(n_features))
        y = x
        xx = x+ts
        yy = y+ts    
        image_data = np.zeros((xx*(n_samples+2)+1, yy*n_chains-1), dtype='uint8')              
                    
        image_data[0:x,:] = my_utils.tile_raster_images(
                                X=init_v, img_shape=(x,y),
                                tile_shape=(1, n_chains), tile_spacing=(ts, ts))
        for idx in range(n_samples):
            # generate `plot_every` intermediate samples that we discard,
            # because successive samples in the chain are too correlated
            # I left a blank row between original images and gibbs samples
            if self.uses_learning_phase:
                vis_prob = sample_fxn(0)
            else:
                vis_prob = sample_fxn()
            image_data[xx*(idx+2) : xx*(idx+2) + x, :] =\
                my_utils.tile_raster_images(X=vis_prob, img_shape=(x, y),
                    tile_shape=(1, n_chains), tile_spacing=(ts, ts))
        
        # save image
        image = Image.fromarray(image_data)
        image.save(filepath)    
    
    
#%%
def ais_estimate_logz(log_w, logZ_base):


    nb_runs = log_w.shape[0]
    r_AIS = logsum(log_w) -  np.log(nb_runs) 
    aa = np.mean(log_w)
    logstd_AIS = np.log(np.std(np.exp(log_w-aa))) + aa - np.log(nb_runs)/2.0
    # Same as computing:
    # logstd_AIS = np.log(np.std(np.exp(log_w))/np.sqrt(nb_runs))     
    
    logZ_est = r_AIS + logZ_base
    
    l_input = np.zeros((2,))
    l_input[0] = np.log(3)+logstd_AIS
    l_input[1] = r_AIS
    
    logZ_est_up = logsum(l_input) + logZ_base
    
    logZ_est_down = logdiff(l_input) + logZ_base
    if np.isnan(logZ_est_down):
        logZ_est_down = 0

    return logZ_est, logZ_est_up, logZ_est_down
    
#%%
class Boltzmann_Res(Boltzmann):
    def __init__(self, output_dim, input_dict = {}, transposed_dict = {},
                 residual_dict = {},
                 mean_field = True, persist = False, centered = False,
                 dropout_p = 0.0, mbs = None, theano_rng = None, **kwargs):
        
        self.residual_dict = residual_dict
        super(Boltzmann_Res,self).__init__(output_dim, input_dict, transposed_dict,
                 mean_field, persist, centered, dropout_p, mbs, theano_rng, **kwargs)
        
        # need to fix z_adj
        if self.layer_type == 'middle':
            len_in = len(self.input_dict) + len(self.residual_dict)
            len_out = len(self.transposed_dict)
            total = len_in + len_out
            # need to compensate for reduced inputs
            self.z_up_adj = 1.0*total/len_in
            self.z_down_adj = 1.0*total/len_out

    def build(self):
        self.build_W()
        super(Boltzmann_Res,self).build_non_W()
        
    def build_W(self):
  
        self.input_index = []
        self.W_ls = []
        for key, input_dim in six.iteritems(self.input_dict):
            if key in self.residual_dict.keys():
                W = self.add_weight((input_dim, self.output_dim),
                                    initializer=keras.initializations.identity,
                                    name='{}-h{}_W'.format(self.name, key),
                                    regularizer = None,
                                    constraint = None,
                                    trainable=False)
            else:
                W = self.add_weight((input_dim, self.output_dim),
                                    initializer=self.init,
                                    name='{}-h{}_W'.format(self.name, key),
                                    regularizer=self.W_regularizer,
                                    constraint=self.W_constraint,
                                    trainable=True)
            
            self.W_ls.append(W)
            self.input_index.append(key)
    
#%%
class DBM_Res(DBM):
    """
    Deep Restricted Boltzmann Machine (DBM) with residual connections.
    """

    def __init__(self, layer_size_list, topology_dict = None,
                 residual_dict = None,
                 mean_field = True, persist = False, centered = False,
                 dropout_prob_list = None,
                 mbs = None, weights_dict = None, theano_rng = None,
                 init_mbs = None, vis_mean = None, **kwargs):  
       
        self.residual_dict = residual_dict
        
        kwargs = super(DBM_Res,self).init_general(layer_size_list, topology_dict,
                                       mean_field, persist, centered, dropout_prob_list,
                                       mbs, theano_rng, init_mbs, vis_mean, **kwargs)
        
        
        self.init_layers(weights_dict, **kwargs)
        
        super(DBM_Res,self).init_values(weights_dict, init_mbs, vis_mean)

    def init_layers(self, weights_dict, **kwargs):

        # initialize the layers
        self.layers_indexs = {}
        self.layers_depths = {}
        order, topology_input_dict = prep_topology(self.topology_dict)
        self.topology_input_dict = topology_input_dict
        _, residual_input_dict = prep_topology(self.residual_dict)
        self.residual_input_dict = residual_input_dict
        depth = 0
        for i in order:
            name = 'h'+str(i)
            if weights_dict is not None:
                weights = self.prep_weights_layer(weights_dict, name)
            else:
                weights = None 

            input_dict = {}
            residual_dict = {}
            if i in self.topology_input_dict.keys():
                for j in list(self.topology_input_dict[i]):
                    input_dict[j] = self.layer_size_list[j]
                    if j in self.residual_input_dict:
                        residual_dict[j] = self.layer_size_list[j]

            transposed_dict = {}
            if i in self.topology_dict.keys():
                for j in self.topology_dict[i]:
                    transposed_dict[j] = self.layers_indexs[j]
                    
            # Only going to center to visible layer since higher ones should 
            # basically be zero and are not worth the effort to compute
            if i == 0:
                centered = self.centered
            else:
                centered = False
                
            layer = Boltzmann_Res(self.layer_size_list[i],
                              input_dict = input_dict,
                              transposed_dict = transposed_dict,
                              residual_dict = residual_dict,
                              mean_field = self.mean_field,
                              persist = self.persist,
                              centered = centered,
                              dropout_p = self.dropout_prob_list[i],
                              mbs = self.mbs,
                              name=name,
                              weights = weights,
                              theano_rng = self.theano_rng,
                              **kwargs)
            
            layer.build()
            
            self.layers_indexs[i] = layer
            self.layers_depths[depth] = layer
            depth += 1
 