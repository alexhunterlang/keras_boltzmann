#%% imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import ast
import copy
import os
import h5py
import numpy as np
from sklearn.metrics import accuracy_score
import keras.backend as K

#%%
def binomial_sample(theano_rng, prob):
    """ Efficient binomial sample in theano """        
    # Note that theano_rng returns dtype int64 by default
    #rnd_sample = theano_rng.uniform(size=prob.shape, dtype=K.floatx())       
    #return T.ceil(prob-rnd_sample)
    sample = theano_rng.binomial(size=prob.shape, n=1, p=prob, dtype=K.floatx())
    return sample

#%%
def calc_acc(prob, lbl, weights = None):
    
    if prob.ndim == 2:
        prob = prob.reshape(prob.shape + (1,))      
    
    acc = np.zeros(prob.shape[2])
    for i in range(prob.shape[2]):
        y_pred = np.argmax(prob[:,:,i], axis = 1)
        acc[i] = accuracy_score(lbl, y_pred, sample_weight = weights)

    return acc

#%%
def standard_folder(folder):
    """ Just ensures that all folders end with a /"""
    if not folder.endswith('/'):
        folder += '/'
    return folder
    
#%%
def concat_fill(a,b, axis=0, fill = np.nan):
    """ Allows concatenation of different shaped arrays """
    
    assert a.ndim == b.ndim    
    
    for i in range(a.ndim):
        if i != axis:
            diff = b.shape[i] - a.shape[i]
            if diff > 0:
                shape = a.shape[0:i] + (diff,)+a.shape[i+1:]
                new_fill = np.empty(shape)
                new_fill[:] = fill
                a = np.concatenate((a,new_fill),axis=i) 
            elif diff < 0:
                diff = -diff
                shape = b.shape[0:i] + (diff,)+b.shape[i+1:]
                new_fill = np.empty(shape)
                new_fill[:] = fill
                b = np.concatenate((b,new_fill),axis=i)
    
    return np.concatenate((a,b),axis=axis)

#%% 
def safe_make_folders(save_folder):
    """ Safely creates folder without overwriting data """

    # If folder doesn't exist, make it
    if not os.path.isdir(save_folder):
        os.makedirs(save_folder)
    else:
        # If folder does exist, check if has other folders
        # other files are okay, for example > out.txt
        dir_list = [ dd for dd in os.listdir(save_folder) 
                        if os.path.isdir(os.path.join(save_folder, dd))]
        
        assert len(dir_list)==0, save_folder+' is not empty'   

#%%
def save_dict(filename,dd):
    """ Given dictionary, saves file with keys and values """
    with open(filename, 'w') as f:
        f.write(str(dd))

#%%
def load_dict(filename):
    """ Given files, reads in dictionary """
    with open(filename,'r') as f:
        dd = ast.literal_eval(f.read())
        
    return dd

#%%
def flatten_nested_dict(input_dict):
    
    output_dict = {}
    HAS_dict = True
    dict_list = [input_dict]
    new_dict_list = []
    while HAS_dict:
        for d in dict_list:
            for key, value in d.items():
                if isinstance(value, dict):
                    new_dict_list.append(value)
                else:
                    output_dict[key] = value
        if len(new_dict_list)==0:
            HAS_dict = False
        else:
            dict_list = copy.deepcopy(new_dict_list)
            new_dict_list = []    
    
    return output_dict

#%%
def load_weights_hd5f(filename):
    """ Loads the weights that are saved in hd5f format and returns a dictionary"""
    
    with h5py.File(filename, 'r') as f:
        keys_ls = f.keys()
        weights = {}
        if keys_ls[0]=='model_weights':
            g = f[keys_ls[0]]
            keys_ls = g.keys()
        else:
            g = f

        for ll in keys_ls:
            for k in g[ll].keys():
                if len(g[ll][k]) > 0:
                    weights[k] = g[ll][k][:]
    
    return weights 
    
#%%
def weights_prep_keras(dict_from_load_weights_hdf5):
    
    dd = dict_from_load_weights_hdf5
    if isinstance(dd.values()[0], list):
        output_dict = dd
    else:
        k = []
        for key in dd.keys():
            if key=='output':
                k.append(key)
            else:
                k.append('_'.join(key.split('_')[0:-1]))
                
        output_dict = {}
        for key in k:
            if key=='output':
                try:
                    W = dd[key+'_W'].astype(np.float32)
                    b = dd[key+'_b'].astype(np.float32)
                except KeyError:     
                    W = dd[key].astype(np.float32)
                    b = None
            else:
                W = dd[key+'_W'].astype(np.float32)
                b = dd[key+'_b'].astype(np.float32)
            output_dict[key] = [W,b]
    
    return output_dict

#%%
def weights_prep_keras_name(dict_from_load_weights_hdf5, layer_name):
    d = dict_from_load_weights_hdf5
    name = layer_name
    
    keys = [k for k in d.keys() if k.startswith(name)]
    assert len(keys)>0
              
    output = []
    try:
        W = d[name+'_W'].astype(np.float32)
        output.append(W)
    except KeyError:
        pass
    try:
        b = d[name+'_b'].astype(np.float32)
        output.append(b)
    except KeyError:
        pass
    
    return output
    
#%%
def orthogonal_init_conv(shape):
    """ This initializes matrices to random orthogonal matrix """
    
    if len(shape)==2:
        num_in, num_out = shape
        b_shape = num_out
    else:
        num_in, num_out = find_display_dim(np.prod(shape))    
        b_shape = shape[0]
        
    W_input = np.random.normal(size=(num_in,num_out))
    
    q,r = np.linalg.qr(W_input)
    if W_input.shape==q.shape:
        W = q
    else:
        W = r
                       
    W = W.reshape(shape)
    b = np.zeros(b_shape,)

    W = W.astype(np.float32)
    b = b.astype(np.float32)

    return W,b    
    
#%% 
def find_display_dim(dim):
    """ Finds dimensions for tiling images """
    x = np.floor(np.sqrt(dim))
    IS_bad = True
    while IS_bad:
        assert x>0
        y = dim/x
        if np.remainder(dim,x)==0:
            IS_bad=False
        else:
            x = x-1
            
    return (int(x),int(y))

#%%
def combine_hdf5(weight_file_ls, filepath):

    # this takes all the hdf5 files in weight_file_ls and makes one single file
    weight_dict = {}
    layer_name = []
    for f in weight_file_ls:
        w = load_weights_hd5f(f)
        for k in w.keys():
            if not k.startswith('output_'):
                k_name = k.encode('utf8')
                layer_name.append(k_name)
                weight_dict[k_name] = w[k]

    f = h5py.File(filepath, 'w')
    f.attrs['layer_names'] = layer_name

    for i, name in enumerate(layer_name):
        g = f.create_group(name)            
        g.attrs['weight_names'] = name
        val = weight_dict[name]
        param_dset = g.create_dataset(name, val.shape,
                                          dtype=val.dtype)
        param_dset[:] = val
    f.flush()
    f.close() 

#%%
def scale_to_unit_interval(ndar, eps=1e-8):
    """ 
    Scales all values in the ndarray ndar to be between 0 and 1 
    Copied from http://deeplearning.net/tutorial/code/utils.py     
    """
    ndar = ndar.copy()
    ndar -= ndar.min()
    ndar *= 1.0 / (ndar.max() + eps)
    return ndar
               
#%%
def tile_raster_images(X, img_shape, tile_shape, tile_spacing=(0, 0),
                       scale_rows_to_unit_interval=True,
                       output_pixel_vals=True):
    """
    Copied from http://deeplearning.net/tutorial/code/utils.py    
    
    Transform an array with one flattened image per row, into an array in
    which images are reshaped and layed out like tiles on a floor.

    This function is useful for visualizing datasets whose rows are images,
    and also columns of matrices for transforming those rows
    (such as the first layer of a neural net).

    :type X: a 2-D ndarray or a tuple of 4 channels, elements of which can
    be 2-D ndarrays or None;
    :param X: a 2-D array in which every row is a flattened image.

    :type img_shape: tuple; (height, width)
    :param img_shape: the original shape of each image

    :type tile_shape: tuple; (rows, cols)
    :param tile_shape: the number of images to tile (rows, cols)

    :param output_pixel_vals: if output should be pixel values (i.e. int8
    values) or floats

    :param scale_rows_to_unit_interval: if the values need to be scaled before
    being plotted to [0,1] or not


    :returns: array suitable for viewing as an image.
    (See:`Image.fromarray`.)
    :rtype: a 2-d array with same dtype as X.

    """

    assert len(img_shape) == 2
    assert len(tile_shape) == 2
    assert len(tile_spacing) == 2

    out_shape = [(ishp + tsp) * tshp - tsp
        for ishp, tshp, tsp in zip(img_shape, tile_shape, tile_spacing)]

    if isinstance(X, tuple):
        assert len(X) == 4
        # Create an output np ndarray to store the image
        if output_pixel_vals:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype='uint8')
        else:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype=X.dtype)

        #colors default to 0, alpha defaults to 1 (opaque)
        if output_pixel_vals:
            channel_defaults = [0, 0, 0, 255]
        else:
            channel_defaults = [0., 0., 0., 1.]

        for i in range(4):
            if X[i] is None:
                # if channel is None, fill it with zeros of the correct
                # dtype
                dt = out_array.dtype
                if output_pixel_vals:
                    dt = 'uint8'
                out_array[:, :, i] = np.zeros(
                    out_shape,
                    dtype=dt
                ) + channel_defaults[i]
            else:
                # use a recurrent call to compute the channel and store it
                # in the output
                out_array[:, :, i] = tile_raster_images(
                    X[i], img_shape, tile_shape, tile_spacing,
                    scale_rows_to_unit_interval, output_pixel_vals)
        return out_array

    else:
        # if we are dealing with only one channel
        H, W = img_shape
        Hs, Ws = tile_spacing

        # generate a matrix to store the output
        dt = X.dtype
        if output_pixel_vals:
            dt = 'uint8'
        out_array = np.zeros(out_shape, dtype=dt)

        for tile_row in range(tile_shape[0]):
            for tile_col in range(tile_shape[1]):
                if tile_row * tile_shape[1] + tile_col < X.shape[0]:
                    this_x = X[tile_row * tile_shape[1] + tile_col]
                    if scale_rows_to_unit_interval:
                        # if we should scale values to be between 0 and 1
                        # do this by calling the `scale_to_unit_interval`
                        # function
                        this_img = scale_to_unit_interval(
                                        this_x.reshape(img_shape))
                    else:
                        this_img = this_x.reshape(img_shape)
                    # add the slice to the corresponding position in the
                    # output array
                    c = 1
                    if output_pixel_vals:
                        c = 255
                    out_array[tile_row * (H + Hs): tile_row * (H + Hs) + H,
                              tile_col * (W + Ws): tile_col * (W + Ws) + W
                              ] = this_img * c
        return out_array            

        
    